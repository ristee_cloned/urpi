<?php

namespace backend\models\search;

use common\models\UserProfile;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{
    public $name;
    public $surname;
    public $phone;
    public $address;
    public $inn;
    public $company;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'status',
                    'created_at',
                    'type',
                ],
                'integer',
            ],
            [
                [
                    'email',
                    'name',
                    'surname',
                    'phone',
                    'address',
                    'inn',
                    'company',
                ],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->joinWith('userProfile');
        $profile = UserProfile::tableName();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => [$profile.'.name' => SORT_ASC],
                    'desc' => [$profile.'.name' => SORT_DESC],
                ],
                'surname' => [
                    'asc' => [$profile.'.surname' => SORT_ASC],
                    'desc' => [$profile.'.surname' => SORT_DESC],
                ],
                'phone' => [
                    'asc' => [$profile.'.phone' => SORT_ASC],
                    'desc' => [$profile.'.phone' => SORT_DESC],
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                ],
                'address' => [
                    'asc' => [$profile.'.address' => SORT_ASC],
                    'desc' => [$profile.'.address' => SORT_DESC],
                ],
                'inn' => [
                    'asc' => [$profile.'.inn' => SORT_ASC],
                    'desc' => [$profile.'.inn' => SORT_DESC],
                ],
                'company' => [
                    'asc' => [$profile.'.company_name' => SORT_ASC],
                    'desc' => [$profile.'.company_name' => SORT_DESC],
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', $profile . '.name', $this->name])
            ->andFilterWhere(['like', $profile . '.surname', $this->surname])
            ->andFilterWhere(['like', $profile . '.phone', $this->phone])
            ->andFilterWhere(['like', $profile . '.address', $this->address])
            ->andFilterWhere(['like', $profile . '.inn', $this->inn])
            ->andFilterWhere(['like', $profile . '.company_name', $this->company]);

        return $dataProvider;
    }
}
