<?php


namespace backend\models;

use common\models\User;
use Yii;

/**
 * Class LoginForm
 *
 * @package backend\models
 */
class LoginForm extends \common\models\LoginForm
{
    private $_user;

    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findAdmin($this->email);
        }

        return $this->_user;
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }
}
