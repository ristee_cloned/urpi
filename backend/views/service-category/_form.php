<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceCategory */
/* @var $image common\models\form\ImageUploadForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::img(
        $model->getImage(),
        ['class' => 'img-responsive']
    ); ?>

    <?= $form->field($image, 'uploadFile')
        ->fileInput()
        ->label('Изображение') ?>

    <?= $form->field($model, 'name')
        ->textInput(
            ['maxlength' => true, 'autofocus' => true]
        ) ?>

    <div class="form-group">
        <?= Html::submitButton(
            'Сохранить',
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
