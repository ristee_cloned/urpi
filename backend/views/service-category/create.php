<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceCategory */
/* @var $image common\models\form\ImageUploadForm */

$this->title = 'Создать категорию услуг';
$this->params['breadcrumbs'][] = [
    'label' => 'Категории услуг',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="service-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'image' => $image,
    ]) ?>

</div>
