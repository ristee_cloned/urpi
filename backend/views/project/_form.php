<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $image common\models\form\ImagesUploadForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?php if ($model->image): ?>
        <?= Html::beginTag('p', ['class' => 'set']) ?>
        <?php foreach ($model->getImage() as $item): ?>
            <?= Html::img(
                $item,
                ['style' => 'width: 100px']
            ) ?>
        <?php endforeach; ?>
        <?= Html::endTag('p') ?>
    <?php else: ?>
        <?= Html::img(
            $model->getImage(),
            ['style' => 'width: 100px']
        ) ?>
    <?php endif; ?>

    <?= $form->field($image, 'uploadFile[]')
        ->fileInput(['multiple' => true])->label('Загрузить изображения') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
