<?php

use common\models\Project;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Портфолио';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Создать проект',
            ['create'], ['class' => 'btn btn-success']
        ) ?>
        <?= Html::a(
            'Сбросить фильтр',
            ['index'], ['class' => 'btn btn-default']
        ) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'content:ntext',
            [
                'attribute' => 'image',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function (Project $data) {
                    $string = Html::beginTag('p', ['class' => 'set']);
                    if ($data->image) {
                        foreach ($data->getImage() as $item) {
                            $string .= Html::img(
                                $item,
                                ['style' => 'width: 50px']
                            );
                        }
                    } else {
                        $string .= Html::img(
                            $data->getImage(),
                            ['style' => 'width: 50px']
                        );
                    }
                    $string .= Html::endTag('p');
                    return $string;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
