<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $image common\models\form\ImagesUploadForm */

$this->title = 'Обновить проект: ' . $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Портфолио',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = [
    'label' => $model->name,
    'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'image' => $image,
    ]) ?>

</div>
