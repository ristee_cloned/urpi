<?php

use common\models\Project;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Портфолио', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Обновить',
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']
        ) ?>
        <?= Html::a(
            'Удалить',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'content:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function (Project $model) {
                    $string = Html::beginTag('p', ['class' => 'set']);
                    if ($model->image) {
                        foreach ($model->getImage() as $item) {
                            $string .= Html::img(
                                $item,
                                ['style' => 'width: 100px']
                            );
                        }
                    } else {
                        $string .= Html::img(
                            $model->getImage(),
                            ['style' => 'width: 100px']
                        );
                    }
                    $string .= Html::endTag('p');
                    return $string;
                }
            ],
        ],
    ]) ?>

</div>
