<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RequestSearch */
/* @var $providerCorporate yii\data\ActiveDataProvider */
/* @var $providerRegular yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <div class="col-md-5 col-sm-4">
        <h3>Корпоративные пользователи</h3>
        <?= ListView::widget([
            'dataProvider' => $providerCorporate,
            'summary' => 'Показано {count} из {totalCount}',
            'options' => ['class' => 'list-group'],
            'itemView' => '_item',
            'layout' => "{summary}\n{items}\n{pager}",
        ]) ?>
    </div>
    <div class="col-md-5 col-sm-4">
        <h3>Розничные пользователи</h3>
        <?= ListView::widget([
            'dataProvider' => $providerRegular,
            'summary' => 'Показано {count} из {totalCount}',
            'options' => ['class' => 'list-group'],
            'itemView' => '_item',
            'layout' => "{summary}\n{items}\n{pager}",
        ]) ?>
    </div>

    <?php Pjax::end(); ?>

</div>
