<?php

/* @var $this \yii\web\View */
/* @var $model common\models\Request */

?>
<div class="client-info">
    <?php if ($model->user->userProfile): ?>
        <p><span>Клиент:</span> <?= $model->user->userProfile->fullName ?></p>
        <p>
            <span>Адрес:</span> <?= $model->user->userProfile->getAddress() ?? ''?>
        </p>
        <p><span>Телефон:</span> <?= $model->user->userProfile->phone ?></p>
        <p><span>E-mail:</span> <?= $model->user->email ?></p>
        <p><span>Дата и время:</span> <?= $model->appointedAt ?></p>
    <?php else: ?>
        <p><span>Клиент:</span> <?= $model->user->email ?></p>
        <p><span>Дата и время:</span> <?= $model->appointedAt ?></p>
    <?php endif; ?>
    <?php if ($model->isCorporate && $model->user->userProfile): ?>
        <hr class="company-info">
        <p>
            <span>
                Компания:
            </span> <?= $model->user->userProfile->company_name ?? '' ?>
        </p>
        <p><span>ИНН:</span> <?= $model->user->userProfile->inn ?? '' ?></p>
    <?php endif; ?>
</div>
<?php if ($model->isClosed): ?>
    <div class="request-info">
        <p><span>Выполнил:</span> <?= $model->executor ?? '' ?></p>
        <p><span>Срок выполнения работ:</span> <?= $model->executionTime ?></p>
    </div>
<?php endif; ?>
