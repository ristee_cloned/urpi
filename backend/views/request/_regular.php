<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\ResultTotal;

/* @var $this \yii\web\View */
/* @var $model common\models\Request */
/* @var $services[] common\models\RequestDetailsRegular */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $total float|int */

?>
<h3>Список услуг</h3>
<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered service',
        ],
        'summary' => '',
        'showFooter' => true,
        'footerRowOptions' => [
            'style' => 'font-weight:bold;',
        ],
        'columns' => [
            [
                'attribute' => 'service_name',
                'footer' => 'Итого, с учетом сидки:',
            ],
            'quantity',
            [
                'attribute' => 'price',
                'label' => 'Цена (руб.)'
            ],
            [
                'attribute' => 'price',
                'format' => 'raw',
                'label' => 'Итого (руб.)',
                'value' => function ($data) {
                    return ResultTotal::countServiceItem($data);
                },
                'footer' => $total,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'request-details-regular',
                'visible' => !$model->isClosed && !$model->isPaid(),
            ],
        ],
    ]); ?>
</div>
<div class="service">
    Счет:
    <?php if ($model->payment): ?>
        <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i>'.($model->isPaid() ? ' Оплачен':''),
            ['payment/view', 'id' => $model->payment->id],
            ['class' => 'btn btn-'.$model->payment->getStatusButtonColor().' pull-right']
        ) ?>
    <?php endif ?>
    <?php if (!$model->isClosed && !$model->isPaid()): ?>
        <?= Html::a(
            'Добавить',
            [
                'request-details-regular/create',
                'requestId' => $model->id,
            ],
            ['class' => 'btn btn-success']
        ) ?>
        <?= Html::a(
            $model->payment ? 'Обновить счет' : 'Выставить счет',
            [
                'payment/create',
                'requestId' => $model->id,
            ],
            ['class' => 'btn btn-default pull-right']
        ) ?>
    <?php endif; ?>
</div>
