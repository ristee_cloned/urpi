<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Request */

$this->title = 'Обновить заявку: ' . $model->id;
$this->params['breadcrumbs'][] = [
    'label' => 'Заявки',
    'url' => ['index']
];
$this->params['breadcrumbs'][] = [
    'label' => 'Заявка: ' . $model->id,
    'url' => ['view', 'id' => $model->id],
];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('_info', [
        'model' => $model,
    ]) ?>

</div>
