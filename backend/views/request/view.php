<?php

use common\models\Request;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\helpers\RequestHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Request */
/* @var $services[] common\models\RequestDetailsRegular */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $total float|int */

$this->title = 'Заявка: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Обновить',
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']
        ) ?>
        <?= Html::a(
            'Удалить',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'label' => 'Номер',
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i'],
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($data) {
                    return RequestHelper::typeLabel($data->type);
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return RequestHelper::statusLabel($data->status);
                },
            ],
            [
                'attribute' => 'status_updated_at',
                'format' => ['date', 'php:d.m.Y H:i'],
            ],
            'vote',
            [
                'attribute' => 'comment',
                'format' => 'html',
                'visible' => $model->isCorporate,
                'value' => function(Request $model) {
                    return nl2br($model->comment);
                },
            ],
            [
                'attribute' => 'images',
                'format' => 'raw',
                'value' => function(Request $model) {
                    $ret = '';
                    foreach ($model->getImages() as $image) {
                        $ret.= Html::a(Html::img($image, ['style' => 'width: 200px']), $image, ['target' => '_blank']);
                    }

                    return $ret;
                }
            ],
        ],
    ]) ?>
    <?php if (!$model->isCorporate): ?>
        <?= $this->render('_regular', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'services' => $services,
            'total' => $total,
        ]) ?>
    <?php endif; ?>
    <?= $this->render('_info', [
        'model' => $model,
    ]) ?>

</div>
