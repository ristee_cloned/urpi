<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\helpers\RequestHelper;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'status')
                ->dropDownList(
                    RequestHelper::statusList(),
                    [
                        'prompt' => 'Выбрать',
                        'disabled' => $model->isClosed ? true : false,
                    ]
                ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'appointedAt')
                ->widget(DateTimePicker::class, [
                    'options' => [
                        'placeholder' => 'Выберите дату и время проведения работ...',
                        'disabled' => $model->isClosed ? true : false,
                    ],
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y H:i',
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'executor')
                ->textInput([
                    'maxlength' => true,
                    'disabled' => $model->isClosed ? true : false,
                ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            'Сохранить',
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
