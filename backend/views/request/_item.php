<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model common\models\Request */

?>
<?= Html::a(
    Html::tag(
        'h4',
        'Заявка: ' . $model->id
    ) . Html::tag(
        'p',
        $model->user->userProfile ?
            $model->user->userProfile->FullName .
            ', ' . $model->user->userProfile->getAddress() : ''
    ),
    ['view', 'id' => $model->id],
    [
        'class' => $model->isNew ?
            'list-group-item new-item' :
            'list-group-item',
    ]
); ?>
