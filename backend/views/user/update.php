<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $profile common\models\UserProfile */

$this->title = 'Обновить пользователя: ' .
    ($profile->fullName ?? $model->email);
$this->params['breadcrumbs'][] = [
        'label' => 'Пользователи', 'url' => ['index']
];
$this->params['breadcrumbs'][] = [
        'label' => $profile->fullName ?? $model->email,
        'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
    ]) ?>

</div>
