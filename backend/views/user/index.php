<?php

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\helpers\UserHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $profile common\models\UserProfile */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Сбросить фильтр',
            ['index'], ['class' => 'btn btn-default']
        ) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-bordered',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'label' => 'Имя',
                'value' => 'userProfile.name',
            ],
            [
                'attribute' => 'surname',
                'label' => 'Фамилия',
                'value' => 'userProfile.surname',
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->userProfile ?
                        Html::a(
                            $data->userProfile->phone,
                            Url::to('tel:' . $data->userProfile->phone)
                        ) : '';
                },
            ],
            'email:email',
            [
                'attribute' => 'address',
                'label' => 'Адрес',
                'value' => function(User $data) {
                    return $data->userProfile ? $data->userProfile->getAddress() : '';
                }
            ],
            [
                'attribute' => 'inn',
                'label' => 'ИНН',
                'value' => 'userProfile.inn',
            ],
            [
                'attribute' => 'company',
                'label' => 'Название компании',
                'value' => 'userProfile.company_name',
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'type',
                    UserHelper::typeList(),
                    ['prompt' => 'Все']
                ),
                'value' => function ($data) {
                    return UserHelper::typeLabel($data->type);
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    UserHelper::statusList(),
                    ['prompt' => 'Все']
                ),
                'value' => function ($data) {
                    return UserHelper::statusLabel($data->status);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
