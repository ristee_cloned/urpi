<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\helpers\UserHelper;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $profile common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')
        ->dropDownList(
            UserHelper::statusList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $form->field($profile, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($profile, 'phone')
        ->widget(MaskedInput::class, [
            'mask' => '+9 (999) 999-99-99',
        ]) ?>

    <?= $form->field($profile, 'address')->textInput() ?>

    <?= $form->field($model, 'type')
        ->dropDownList(
            UserHelper::typeList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $form->field($profile, 'inn')
        ->widget(MaskedInput::class, [
            'mask' => '999999999999',
        ]) ?>

    <?= $form->field($profile, 'company_name')->textInput() ?>

    <?= $form->field($profile, 'discount_card')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
