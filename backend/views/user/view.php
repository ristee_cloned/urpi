<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\helpers\UserHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $profile common\models\UserProfile */

$this->title = $profile->fullName ?? $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Обновить',
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']
        ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return UserHelper::statusLabel($data->status);
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'dd.MM.Y'],
            ],
            'userProfile.name',
            'userProfile.surname',
            [
                'attribute' => 'phone',
                'label' => 'Телефон',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->userProfile ?
                        Html::a(
                            $data->userProfile->phone,
                            Url::to('tel:' . $data->userProfile->phone)
                        ) : '';
                },
            ],
            'email:email',
            [
                'attribute' => 'userProfile.address',
                'value' => $model->userProfile ? $model->userProfile->getAddress() : '',
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($data) {
                    return UserHelper::typeLabel($data->type);
                }
            ],
            'userProfile.inn',
            'userProfile.company_name',
            'userProfile.discount_card',
        ],
    ]) ?>

</div>
