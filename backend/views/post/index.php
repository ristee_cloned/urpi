<?php

use common\models\Post;
use yii\helpers\{
    Html,
    StringHelper
};
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'tableOptions' => [
                'class' => 'table table-striped table-bordered',
            ],
            'options' => [
                'class' => 'table-responsive',
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'image',
                    'label' => false,
                    'format' => 'raw',
                    'value' => function (Post $data) {
                        return Html::img(
                            $data->getImage(),
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:d.m.Y H:i'],
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'language' => 'ru',
                        'convertFormat' => true,
                        'startAttribute' => 'date_from',
                        'endAttribute' => 'date_to',
                        'pluginOptions' => [
                            'timePicker' => true,
                            'timePickerIncrement' => 15,
                            'locale' => [
                                'format' => 'd.m.Y H:i'
                            ]
                        ]
                    ]),
                ],
                'title',
                [
                    'attribute' => 'content',
                    'format' => 'raw',
                    'value' => function ($searchModel) {
                        return StringHelper::truncate($searchModel->content, 240);
                    },
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>


    <?php Pjax::end(); ?>

</div>
