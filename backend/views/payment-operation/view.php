<?php

use common\models\PaymentOperation;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentOperation */

$this->title = "Операция по счету {$model->payment_id} #".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Операции по счетам', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payment-operation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'payment_id',
                'format' => 'html',
                'value' => function(PaymentOperation $model) {
                    return Html::a('Счет #'.$model->payment_id, ['payment/view', 'id' => $model->payment_id]);
                }
            ],
            'out_sum:currency',
            'signature',
            'description',
            'status',
            'created_at:datetime',
            'updated_at:datetime',
            'url:url',
        ],
    ]) ?>

</div>
