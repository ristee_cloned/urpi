<?php

use common\models\PaymentOperation;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Операции по счетам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-operation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'payment_id',
                'format' => 'html',
                'value' => function(PaymentOperation $model) {
                    return Html::a('Счет #'.$model->payment_id, ['payment/view', 'id' => $model->payment_id]);
                }
            ],
            'out_sum:currency',
//            'signature',
//            'description',
            'status',
            'created_at:datetime',
            'updated_at:datetime',
            //'url:url',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>


</div>
