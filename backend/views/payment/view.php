<?php

use common\models\Payment;
use common\models\Project;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = 'Счет на зяавку № '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['request/index']];
$this->params['breadcrumbs'][] = ['label' => 'Заявка #'.$model->id, 'url' => ['request/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'label' => 'Номер заявки',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'paid_at:datetime',
            [
                'attribute' => 'status',
                'value' => function(Payment $model) {
                    return $model->getStatusLabel();
                }
            ],
            'total_price',
        ],
    ]) ?>

</div>
