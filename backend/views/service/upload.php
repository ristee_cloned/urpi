<?php

use yii\widgets\ActiveForm;

/** @var $model common\models\form\ExcelUploadForm */

$this->title = 'Загрузить каталог';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="service-upload">
    <?php $form = ActiveForm::begin(['id' => 'upload-form']) ?>

    <?= $form->field($model, 'uploadFile')->fileInput() ?>

    <button class="btn btn-primary">Импортировать</button>

    <?php ActiveForm::end() ?>
</div>
