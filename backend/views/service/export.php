<?php

use moonland\phpexcel\Excel;

/* @var $services common\models\Service */

Excel::export([
    'models' => $services,
    'asAttachment' => true,
    'fileName' => 'catalog',
    'autoSize' => true,
    'columns' => [
        'id',
        'serviceCategory.name:text',
        'name',
        'measure',
        'price_min',
        'price_max',
    ],
    'headers' => [
        'serviceCategory.name' => 'Category',
        'name' => 'Name',
        'measure' => 'Measure',
        'price_min' => 'Price-min',
        'price_max' => 'Price-max',
    ],
]);
