<?php

use common\models\Service;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories \common\models\ServiceCategory[] */

$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Создать',
            ['create'],
            ['class' => 'btn btn-success']
        ) ?>
        <?= Html::a(
            'Сбросить фильтр',
            ['index'], ['class' => 'btn btn-default']
        ) ?>
        <?= Html::a(
            'Импорт',
            ['upload'],
            ['class' => 'btn btn-primary']
        ) ?>
        <?= Html::a(
            'Экспорт',
            ['export'],
            ['class' => 'btn btn-primary']
        ) ?>
    </p>

    <?php Pjax::begin(); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'tableOptions' => [
                'class' => 'table table-striped table-bordered',
            ],
            'options' => [
                'class' => 'table-responsive',
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'service_category_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'service_category_id',
                        $categories,
                        [
                            'prompt' => 'Все',
                        ]
                    ),
                    'value' => function ($data) {
                        return $data->serviceCategory ?
                            $data->serviceCategory->name : '';
                    }
                ],
                'name',
                'description',
                //'content:ntext',
                'measure',
                'price_min',
                'price_max',
                [
                    'attribute' => 'image',
                    'label' => false,
                    'format' => 'raw',
                    'value' => function (Service $data) {
                        return Html::img(
                            $data->getImage(),
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>

    <?php Pjax::end(); ?>

</div>
