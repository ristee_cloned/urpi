<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $image common\models\form\ImageUploadForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories common\models\ServiceCategory[] */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::img(
        $model->getImage(),
        ['class' => 'img-responsive']
    ); ?>

    <?= $form->field($image, 'uploadFile')
        ->fileInput()
        ->label('Изображение') ?>

    <?= $form
        ->field($model, 'service_category_id')
        ->dropDownList($categories, ['prompt' => 'Выбрать']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price_min')->textInput() ?>

    <?= $form->field($model, 'price_max')->textInput() ?>

    <?= $form->field($model, 'measure')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
