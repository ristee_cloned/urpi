<?php

use common\models\Service;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Service */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Обновить',
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary']
        ) ?>
        <?= Html::a(
            'Удалить',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]
        ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'service_category_id',
                'value' => function ($data) {
                    return $data->serviceCategory ?
                        $data->serviceCategory->name : '';
                }
            ],
            'name',
            'description',
            'content:ntext',
            'measure',
            'price_min',
            'price_max',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function (Service $model) {
                    return Html::img(
                         $model->getImage(),
                        [
                            'class' => 'img-responsive',
                            'width' => 300,
                        ]
                    );
                }
            ],
        ],
    ]) ?>

</div>
