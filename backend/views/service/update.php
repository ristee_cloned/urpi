<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $image common\models\form\ImageUploadForm */
/* @var $categories common\models\ServiceCategory[] */

$this->title = 'Обновить услугу: ' . $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Услуги',
    'url' => ['index']
];
$this->params['breadcrumbs'][] = [
    'label' => $model->name,
    'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'image' => $image,
    ]) ?>

</div>
