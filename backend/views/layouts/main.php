<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
$controller = Yii::$app->controller->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выйти (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container content-container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <div class="row">
            <?php if (!Yii::$app->user->isGuest): ?>
            <div class="col-md-2 col-sm-3">
                <div class="list-group">
                    <?= Html::a(
                        '<span>Заявки</span>',
                        '/request/index',
                        [
                            'class' => $controller == 'request' ||
                            $controller == 'request-details-regular' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>О компании</span>',
                        '/about/index',
                        [
                            'class' => $controller == 'about' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Новости</span>',
                        '/post/index',
                        [
                            'class' => $controller == 'post' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Пользователи</span>',
                        '/user/index',
                        [
                            'class' => $controller == 'user' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Портфолио</span>',
                        '/project/index',
                        [
                            'class' => $controller == 'project' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Каталог / Категории</span>',
                        '/service-category/index',
                        [
                            'class' => $controller == 'service-category' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Каталог / Услуги</span>',
                        '/service/index',
                        [
                            'class' => $controller == 'service' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                    <?= Html::a(
                        '<span>Пуши</span>',
                        '/push',
                        [
                            'class' => $controller == 'push' ?
                                'list-group-item active' :
                                'list-group-item ',
                        ]
                    ) ?>
                </div>
            </div>
            <?php endif ?>
            <div class="col-md-10 col-sm-9">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
