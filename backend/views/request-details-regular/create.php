<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestDetailsRegular */
/* @var $requestId integer */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = [
    'label' => 'Заявка: ' . $requestId,
    'url' => ['request/view', 'id' => $model->request->id],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-details-regular-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
