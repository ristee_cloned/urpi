<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RequestDetailsRegular */
/* @var $requestId integer */

$this->title = 'Обновить: ' . $model->service_name;
$this->params['breadcrumbs'][] = [
    'label' => 'Заявка: ' . $requestId,
    'url' => ['request/view', 'id' => $model->request->id],
];
$this->params['breadcrumbs'][] = ['label' => $model->service_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="request-details-regular-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
