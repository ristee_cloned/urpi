<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'PUSH сообщения для всех';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success', 'data' => ['method' => 'post', 'confimr' => 'Вы уверены, что хотите отправить PUSH для всех?']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

