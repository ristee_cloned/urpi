<?php


namespace backend\controllers;


use backend\models\PushForm;
use yii\web\Controller;

class PushController extends AdminController
{
    public function actionIndex()
    {
        $model = new PushForm();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->send()) {
                    \Yii::$app->session->setFlash('success', 'PUSH сообщение успешно отправлено');
                    return $this->refresh();
                };
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
