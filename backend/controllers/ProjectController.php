<?php

namespace backend\controllers;

use Yii;
use common\models\Project;
use common\models\form\ImagesUploadForm;
use backend\models\search\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];

        return $parent;
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        $image = new ImagesUploadForm(
            Yii::$app->params['portfolio.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = new ImagesUploadForm(
            Yii::$app->params['portfolio.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param Project $model
     * @param ImagesUploadForm $image
     * @return bool
     */
    protected function processModels($model, $image)
    {
        if (Yii::$app->request->isPost) {
            $image->uploadFile = UploadedFile::getInstances(
                $image,
                'uploadFile'
            );

            if ($model->load(Yii::$app->request->post())) {
                if ($relImagePaths = $image->upload()) {
                    if (is_array($model->image)) {
                        $model->deleteImagesFiles($model->image);
                    }
                    $resizeRelPaths = $image->resize(
                        $relImagePaths,
                        Yii::$app->params['portfolio.imageWidth'],
                        Yii::$app->params['portfolio.imageHeight']
                    );
                    $model->image = $resizeRelPaths ?? $relImagePaths;
                }

                if ($model->save()) {
                    return true;
                }
            }
        }

        return false;
    }
}
