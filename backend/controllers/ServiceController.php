<?php

namespace backend\controllers;

use common\models\form\ImageUploadForm;
use common\models\form\ExcelUploadForm;
use common\models\ServiceCategory;
use Yii;
use common\models\Service;
use backend\models\search\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];

        return $parent;
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => ServiceCategory::getAllNamesWithIds(),
        ]);
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful,
     * the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();
        $image = new ImageUploadForm(
            Yii::$app->params['service.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'categories' => ServiceCategory::getAllNamesWithIds(),
            'image' => $image,
        ]);
    }

    /**
     * Updates an existing Service model.
     * If update is successful,
     * the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = new ImageUploadForm(
            Yii::$app->params['service.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categories' => ServiceCategory::getAllNamesWithIds(),
            'image' => $image,
        ]);
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful,
     * the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Exports data to a file
     */
    public function actionExport()
    {
        $services = Service::find()->all();

        $this->render('export', [
            'services' => $services,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionUpload()
    {
        $file = Yii::getAlias('@backend') .
            Yii::$app->params['service.importFilePath'];
        $model = new ExcelUploadForm($file);

        if (Yii::$app->request->isPost) {
            $model->uploadFile = UploadedFile::getInstance($model, 'uploadFile');

            if ($model->upload()) {
                if (Service::import($file)) {
                    Yii::$app
                        ->session
                        ->setFlash('success', 'Данные импортированы.');
                } else {
                    Yii::$app
                        ->session
                        ->setFlash('error', 'Ошибка импорта данных.');
                }
                return $this->redirect('index');
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(
            'The requested page does not exist.'
        );
    }

    /**
     * @param Service $model
     * @param ImageUploadForm $image
     * @return bool
     */
    protected function processModels($model, $image)
    {
        if (Yii::$app->request->isPost) {
            $image->uploadFile = UploadedFile::getInstance(
                $image,
                'uploadFile'
            );

            if ($model->load(Yii::$app->request->post())) {
                if ($relImagePath = $image->upload()) {
                    $model->deleteImageFile($model->image);
                    $resizeRelPath = $image->resize(
                        $relImagePath,
                        Yii::$app->params['service.imageWidth'],
                        Yii::$app->params['service.imageHeight']
                    );
                    $model->image = $resizeRelPath ?? $relImagePath;
                }
                if ($model->save()) {
                    return true;
                }
            }
        }

        return false;
    }
}
