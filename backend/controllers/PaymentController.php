<?php

namespace backend\controllers;

use common\models\Payment;
use common\models\Request;
use common\models\User;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class PaymentController
 * @package backend\controllers
 */
class PaymentController extends Controller
{
    /**
     * @param $requestId
     * @return string|\yii\web\Response
     */
    public function actionCreate($requestId)
    {
        $payment = Payment::findOne($requestId);

        if ($payment) {
            if ($payment->getIsPaid()) {
                Yii::$app->session
                    ->setFlash('error', 'Нельзя выставить оплаченный счет.');
                return $this->redirect(['request/view', 'id' => $requestId]);
            }
            $request = $payment->request;
            $payment->total_price = $request->calculateTotalCost();
        }

        if (!$payment) {
            $request = Request::findOne($requestId);
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $payment = new Payment($request, $user);
            $payment->id = $requestId;
            $payment->total_price = $payment->getReq()->calculateTotalCost();
        }

        if ($payment->save()) {
            Yii::$app->session->setFlash('success', 'Счет выставлен.');
        }
        return $this->redirect(['request/view', 'id' => $requestId]);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $payment = $this->findPayment($id);

        return $this->render('view', [
            'model' => $payment,
        ]);
    }

    /**
     * @param $id
     * @return Payment|null
     * @throws NotFoundHttpException
     */
    protected function findPayment($id)
    {
        $model = Payment::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        return $model;
    }
}
