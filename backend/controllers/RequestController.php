<?php

namespace backend\controllers;

use Yii;
use common\models\Request;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent['verbs'] = [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
        ];

        return $parent;
    }

    /**
     * Lists all Request models,
     * with separation into corporate and regular.
     * @return mixed
     */
    public function actionIndex()
    {
        $providerRegular = new ActiveDataProvider([
            'query' => Request::getAllRegular(),
            'pagination' => [
                'pageParam' => 'page-reg',
                'pageSize' => 5,
            ],
        ]);
        $providerCorporate = new ActiveDataProvider([
            'query' => Request::getAllCorporate(),
            'pagination' => [
                'pageParam' => 'page-cor',
                'pageSize' => 5,
            ],
        ]);

        return $this->render('index', [
            'providerCorporate' => $providerCorporate,
            'providerRegular' => $providerRegular,
        ]);
    }

    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $total = $model->calculateTotalCost();

        if ($model && !$model->getIsCorporate()) {
            $dataProvider = new ActiveDataProvider([
                'query' => $model->getRequestDetailsRegular(),
                'sort' => false,
                'pagination' => false,
            ]);
        }

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider ?? [],
            'services' => $model->services,
            'total' => $total,
        ]);
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
