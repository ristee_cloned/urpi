<?php

namespace backend\controllers;

use common\models\form\ImageUploadForm;
use Yii;
use common\models\ServiceCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ServiceCategoryController implements the CRUD actions for ServiceCategory model.
 */
class ServiceCategoryController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'delete' => ['POST'],
            ],
        ];

        return $parent;
    }

    /**
     * Lists all ServiceCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ServiceCategory::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ServiceCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServiceCategory model.
     * If creation is successful,
     * the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServiceCategory();
        $image = new ImageUploadForm(
            Yii::$app->params['category.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    /**
     * Updates an existing ServiceCategory model.
     * If update is successful,
     * the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = new ImageUploadForm(
            Yii::$app->params['category.imageRelPath']
        );

        if ($this->processModels($model, $image)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    /**
     * Deletes an existing ServiceCategory model.
     * If deletion is successful,
     * the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ServiceCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServiceCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(
            'The requested page does not exist.'
        );
    }

    /**
     * @param ServiceCategory $model
     * @param ImageUploadForm $image
     * @return bool
     */
    protected function processModels($model, $image)
    {
        if (Yii::$app->request->isPost) {
            $image->uploadFile = UploadedFile::getInstance(
                $image,
                'uploadFile'
            );

            if ($model->load(Yii::$app->request->post())) {
                if ($relImagePath = $image->upload()) {
                    $model->deleteImageFile($model->image);
                    $resizeRelPath = $image->resize(
                        $relImagePath,
                        Yii::$app->params['category.imageWidth'],
                        Yii::$app->params['category.imageHeight']
                    );
                    $model->image = $resizeRelPath ?? $relImagePath;
                }
                if ($model->save()) {
                    return true;
                }
            }
        }

        return false;
    }
}
