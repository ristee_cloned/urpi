<?php

namespace backend\controllers;

use Yii;
use common\models\RequestDetailsRegular;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestDetailsRegularController implements the CRUD actions for RequestDetailsRegular model.
 */
class RequestDetailsRegularController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single RequestDetailsRegular model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'requestId' => $model->request->id,
        ]);
    }

    /**
     * Creates a new RequestDetailsRegular model.
     * If creation is successful, the browser will be redirected to the request 'view' page.
     * @param int $requestId
     * @return mixed
     * @throws NotFoundHttpException if the request model cannot be found
     */
    public function actionCreate($requestId)
    {
        $model = new RequestDetailsRegular();
        $model->request_id = $requestId;

        if (!$model->request) {
            throw new NotFoundHttpException(
                'Нельзя создать услугу, заявка не существует!'
            );
        }
        if ($model->request->getIsClosed()) {
            Yii::$app->session->setFlash(
                'error',
                'Нельзя создать услугу, заявка закрыта!'
            );
            return $this->redirect(['request/view', 'id' => $model->request->id]);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'request/view',
                'id' => $model->request->id,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'requestId' => $model->request->id,
        ]);
    }

    /**
     * Updates an existing RequestDetailsRegular model.
     * If update is successful, the browser will be redirected to the request 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'request/view',
                'id' => $model->request->id,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'requestId' => $model->request->id,
        ]);
    }

    /**
     * Deletes an existing RequestDetailsRegular model.
     * If deletion is successful, the browser will be redirected to the request 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['request/view', 'id' => $model->request->id]);
    }

    /**
     * Finds the RequestDetailsRegular model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RequestDetailsRegular the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequestDetailsRegular::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрашиваемая страница не существует.');
    }
}
