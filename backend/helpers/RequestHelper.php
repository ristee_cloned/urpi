<?php

namespace backend\helpers;

use common\models\Request;
use yii\helpers\Html;

/**
 * Class RequestHelper
 * @package backend\helpers
 */
class RequestHelper extends UserHelper
{
    /**
     * Returns an array of pairs (status value => name).
     * @return array
     */
    public static function statusList(): array
    {
        return [
            Request::STATUS_NEW => 'НОВАЯ',
            Request::STATUS_CONFIRMED => 'ПОДТВЕРЖДЕНА',
            Request::STATUS_IN_WORK => 'В РАБОТЕ',
            Request::STATUS_PAID => 'ОПЛАЧЕНА',
            Request::STATUS_CLOSED => 'ЗАКРЫТА',
            Request::STATUS_REJECTED => 'ОТКЛОНЕНА',
        ];
    }

    /**
     * Returns a span tag with a value and class
     * depending on the status passed.
     * @param $status
     * @return string
     */
    public static function statusLabel($status): string
    {
        switch ($status) {
            case Request::STATUS_NEW:
                $class = 'label label-success';
                break;
            case Request::STATUS_REJECTED:
                $class = 'label label-danger';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag(
            'span',
            self::statusName($status),
            ['class' => $class]
        );
    }
}
