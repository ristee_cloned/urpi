<?php

namespace backend\helpers;

use common\models\User;
use yii\helpers\Html;

/**
 * Class UserHelper
 * @package backend\helpers
 */
class UserHelper extends Helper
{
    /**
     * Returns an array of pairs (status value => name).
     * @return array
     */
    public static function statusList(): array
    {
        return [
            User::STATUS_DELETED => 'ЗАБЛОКИРОВАН',
            User::STATUS_INACTIVE => 'НЕПОДТВЕРЖДЕН',
            User::STATUS_ACTIVE => 'АКТИВНЫЙ',
        ];
    }

    /**
     * Returns a span tag with a value and class depending on the status passed.
     * @param $status
     * @return string
     */
    public static function statusLabel($status): string
    {
        switch ($status) {
            case User::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case User::STATUS_DELETED:
                $class = 'label label-danger';
                break;
            case User::STATUS_INACTIVE:
                $class = 'label label-warning';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag(
            'span',
            self::statusName($status),
            ['class' => $class]
        );
    }

    /**
     * Returns an array of pairs (type value => name).
     * @return array
     */
    public static function typeList(): array
    {
        return [
            User::TYPE_REGULAR => 'ОБЫЧНЫЙ',
            User::TYPE_CORPORATE => 'КОРПОРАТИВНЫЙ',
            User::TYPE_ADMIN => 'АДМИНИСТРАТОР',
        ];
    }

    /**
     * Returns a span tag with a value and class depending on the type passed.
     * @param $type
     * @return string
     */
    public static function typeLabel($type): string
    {
        switch ($type) {
            case User::TYPE_REGULAR:
                $class = 'label label-success';
                break;
            case User::TYPE_CORPORATE:
                $class = 'label label-primary';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag(
            'span',
            self::typeName($type),
            ['class' => $class]
        );
    }
}
