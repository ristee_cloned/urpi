<?php

namespace backend\helpers;

use yii\helpers\ArrayHelper;

/**
 * Class Helper
 * @package backend\helpers
 */
abstract class Helper
{
    /**
     * Returns a list of statuses.
     * @return array
     */
    abstract public static function statusList(): array;

    /**
     * Returns the name of the status depending on the status value passed.
     * @param $status
     * @return string
     */
    protected static function statusName($status): string
    {
        return ArrayHelper::getValue(static::statusList(), $status);
    }

    /**
     * Returns status label depending on the status passed.
     * @param $status
     * @return string
     */
    abstract public static function statusLabel($status): string;

    /**
     * Returns a list of types.
     * @return array
     */
    abstract public static function typeList(): array;

    /**
     * Returns the name of the type depending on the type value passed.
     * @param $type
     * @return string
     */
    protected static function typeName($type): string
    {
        return ArrayHelper::getValue(static::typeList(), $type);
    }

    /**
     * Returns type label depending on the type passed.
     * @param $type
     * @return string
     */
    abstract public static function typeLabel($type): string;
}
