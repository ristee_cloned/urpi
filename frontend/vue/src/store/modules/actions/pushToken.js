const actions = {
    GET_PUSHTOKEN: async context => {
        try {
            // eslint-disable-next-line no-undef
            window.webkit.messageHandlers.getPushTokenFromSwift.postMessage(context.getters.TOKEN);
            // eslint-disable-next-line no-empty
        } catch (error) {}

        try {
            // eslint-disable-next-line no-undef
            JSInterface.updatePushToken(context.getters.TOKEN);
            // eslint-disable-next-line no-empty
        } catch (error) {}
    }
};

export default {
    actions
};