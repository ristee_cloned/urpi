import Axios from "axios";
import error from "../error";

const state = {
    feedback: null
};

const getters = {
    FEEDBACK: state => state.feedback
};

const mutations = {
    SET_FEEDBACK: (state, payload) => {
        state.feedback = payload;
    }
};

const actions = {
    SET_FEEDBACK: async (context, payload) => {
        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/user/feedback`,
            method: "POST",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`,
                "Content-Type": "application/json"
            },
            data: JSON.stringify(payload)
        })
            .then(response => {
                const data = response.data;

                context.commit("SET_AWAITRESPONSE", false);

                if (response.status == 200) {
                    context.commit("SET_FEEDBACK", data.success);
                }
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
