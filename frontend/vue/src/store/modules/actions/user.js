import Axios from "axios";
import error from "../error";

const state = {
    user: {
        name: null,
        surname: null,
        type: null,
        phone: null,
        address: null
    }
};

const getters = {
    USER: state => state.user
};

const mutations = {
    SET_USER: (state, payload) => {
        state.user = payload;
    }
};

const actions = {
    GET_USER: async context => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/user-profile`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_USER", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    UPDATE_USER: async (context, payload) => {
        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/user-profile`,
            method: "post",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`,
                "Content-Type": "application/json"
            },
            data: JSON.stringify(payload)
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_USER", data);

                    context.dispatch("SHOW_ALERT", {
                        message: "Данные успешно обновлены"
                    });
                }

                context.commit("SET_AWAITRESPONSE", false);
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
