import Axios from "axios";
import error from "../error";

const actions = {
    PUSH_RESENDPASSWORD: async (context, payload) => {
        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/user/request-password-reset`,
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(payload)
            })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.dispatch("SHOW_ALERT", {
                        message: data.message
                    });
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    actions
};