import Axios from "axios";
import error from "../error";

const state = {
  newsList: {
    data: null,
    page: null,
    size: null,
  },
  news: null,
};

const getters = {
  NEWSLIST: (state) => state.newsList,

  NEWS: (state) => state.news,
};

const mutations = {
  SET_NEWSLIST: (state, payload) => {
    if (state.newsList) {
      const oldList = state.newsList.data || new Array();
      const newData = payload.data;
      const newList = payload.prepend
        ? newData.concat(oldList)
        : oldList.concat(newData);

      state.newsList.data = newList;
      state.newsList.page = payload.page;
      state.newsList.size = payload.size;
    } else {
      state.newsList = payload;
    }
  },

  REFRESH_NEWSLIST: (state, payload) => {
    state.newsList = payload;
  },

  SET_NEWS: (state, payload) => {
    state.news = payload;
  },
};

const actions = {
  GET_NEWSLIST: async (context, payload) => {
    const currentPage = payload.page || 1;
    const refreshList = payload.refresh || false;
    const prepend = payload.prepend || false;

    context.commit("SET_AWAITRESPONSE", true);

    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/post?page=${currentPage}&sort=-id`,
      method: "get",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((response) => {
        const data = response.data;

        const parameters = {
          data,
          prepend,
          size: response.headers["x-pagination-page-count"],
          page: response.headers["x-pagination-current-page"],
        };

        if (response.status == 200) {
          if (!refreshList) {
            if (
              currentPage != state.newsList.page &&
              (currentPage != 1 || prepend)
            ) {
              context.commit("SET_NEWSLIST", parameters);
            } else if (currentPage == 1 && !prepend) {
              context.commit("REFRESH_NEWSLIST", parameters);
            }
          } else {
            context.commit("REFRESH_NEWSLIST", parameters);
          }

          context.commit("SET_AWAITRESPONSE", false);
        }
      })
      .catch((err) => {
        context.commit("SET_AWAITRESPONSE", false);
        error.errorResponse(err.response, context.dispatch);
      });
  },

  GET_NEWS: async (context, payload) => {
    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/post/${payload}`,
      method: "get",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
      },
    })
      .then((response) => {
        const data = response.data;

        if (response.status == 200) {
          context.commit("SET_NEWS", data);
        }
      })
      .catch((err) => {
        error.errorResponse(err.response, context.dispatch);
      });
  },

  SET_NEWS: (context, payload) => {
    context.commit("SET_NEWS", payload);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
