import Axios from "axios";
import error from "../error";

const state = {
    invoices: {
        data: null,
        page: null,
        size: null
    },

    invoice: {
        id: null,
        created_at: null,
        appointed_at: null,
        status: null,
        status_updated_at: null,
        executor: null,
        vote: null,
        total: null,
        images: new Array(),
        comment: null,
        services: new Array()
    },

    invoiceStatus: {
        link: null
    }
};

const getters = {
    INVOICES: state => state.invoices,

    INVOICE: state => state.invoice,

    INVOICESTATUS: state => state.invoiceStatus
};

const mutations = {
    SET_INVOCES: (state, payload) => {
        if (state.invoices) {
            const oldList = state.invoices.data || new Array();
            const newData = payload.data;
            const ordersList = payload.prepend ?
                newData.concat(oldList) :
                oldList.concat(newData);

            state.invoices.data = ordersList;
            state.invoices.page = payload.page;
            state.invoices.size = payload.size;
        } else {
            state.invoices = payload;
        }
    },

    REFRESH_INVOICES: (state, payload) => {
        state.invoices = payload;
    },

    SET_INVOICE: (state, payload) => {
        state.invoice = payload;
    },

    SET_INVOICESTATUS: (state, payload) => {
        state.invoiceStatus = payload;
    }
};

const actions = {
    GET_INVOICES: async (context, payload) => {
        const currentPage = payload.page || 1;
        const refreshList = payload.refresh || false;
        const prepend = payload.prepend || false;

        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/payment?page=${currentPage}&sort=-id`,
                method: "get",
                headers: {
                    Authorization: `Bearer ${context.getters.TOKEN}`,
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
            .then(response => {
                const data = response.data;
                const parameters = {
                    data,
                    prepend,
                    size: response.headers["x-pagination-page-count"],
                    page: response.headers["x-pagination-current-page"]
                };

                if (response.status == 200) {
                    if (!refreshList) {
                        if (
                            currentPage != state.orders.page &&
                            (currentPage != 1 || prepend)
                        ) {
                            context.commit("SET_INVOICES", parameters);
                        } else if (currentPage == 1 && !prepend) {
                            context.commit("REFRESH_INVOICES", parameters);
                        }
                    } else {
                        context.commit("REFRESH_INVOICES", parameters);
                    }

                    context.commit("SET_AWAITRESPONSE", false);
                }
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    },

    GET_INVOICE: async (context, payload) => {
        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/payment/${payload}`,
                method: "get",
                headers: {
                    Authorization: `Bearer ${context.getters.TOKEN}`
                }
            })
            .then(response => {
                const data = response.data;
                context.commit("SET_AWAITRESPONSE", false);
                if (response.status == 200) {
                    context.commit("SET_INVOICE", data);
                }
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    },

    PUSH_INVOICE: async (context, payload) => {
        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/payment/pay/${payload}`,
                method: "POST",
                headers: {
                    Authorization: `Bearer ${context.getters.TOKEN}`,
                    "Content-Type": "application/json"
                }
            })
            .then(response => {
                const data = response.data;
                context.commit("SET_AWAITRESPONSE", false);

                if (response.status == 200) {
                    window.open(data);
                    // context.commit("SET_INVOICESTATUS", {
                    //     link: data
                    // });
                }
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    },

    SET_INVOICESTATUS: (context, payload) => {
        context.commit("SET_INVOICESTATUS", payload);
    },

    SET_INVOICE: (context, payload) => {
        context.commit("SET_INVOICE", payload);
    },

    PUSH_PAYSTATUS: async (context, payload) => {
        context.commit("SET_AWAITRESPONSE", true);

        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/payment/${payload.path}`,
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(payload.data)
            })
            .then(response => {
                // const data = response.data;
                context.commit("SET_AWAITRESPONSE", false);

                if (response.status == 200) {
                    // console.log(data);
                }
            })
            .catch(err => {
                context.commit("SET_AWAITRESPONSE", false);
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};