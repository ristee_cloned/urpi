import Axios from "axios";
import error from "../error";

const state = {
    projectList: null,
    project: null
};

const getters = {
    PROJECTLIST: state => state.projectList,

    PROJECT: state => state.project
};

const mutations = {
    SET_PROJECTLIST: (state, payload) => {
        state.projectList = payload;
    },

    SET_PROJECT: (state, payload) => {
        state.project = payload;
    }
};

const actions = {
    GET_PROJECTLIST: async context => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/project`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_PROJECTLIST", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    GET_PROJECT: async (context, payload) => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/project/${payload}`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_PROJECT", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    SET_PROJECT: (context, payload) => {
        context.commit("SET_PROJECT", payload);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
