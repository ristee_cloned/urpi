import Axios from "axios";
import error from "../error";

const state = {
  orders: {
    data: null,
    page: null,
    size: null,
  },

  order: {
    id: null,
    created_at: null,
    appointed_at: null,
    status: null,
    status_updated_at: null,
    executor: null,
    vote: null,
    total: null,
    images: new Array(),
    comment: null,
    services: new Array(),
  },

  orderStatus: {
    response: true,
    success: null,
    data: null,
  },

  vote: null,
};

const getters = {
  ORDERS: (state) => state.orders,

  ORDER: (state) => state.order,

  ORDERSTATUS: (state) => state.orderStatus,

  VOTE: (state) => state.vote,
};

const mutations = {
  SET_ORDERS: (state, payload) => {
    if (state.orders) {
      const oldList = state.orders.data || new Array();
      const newData = payload.data;
      const ordersList = payload.prepend
        ? newData.concat(oldList)
        : oldList.concat(newData);

      state.orders.data = ordersList;
      state.orders.page = payload.page;
      state.orders.size = payload.size;
    } else {
      state.orders = payload;
    }
  },

  REFRESH_ORDERS: (state, payload) => {
    state.orders = payload;
  },

  SET_ORDER: (state, payload) => {
    state.order = payload;
  },

  SET_ORDERSTATUS: (state, payload) => {
    state.orderStatus = payload;
  },

  SET_VOTE: (state, payload) => {
    state.vote = payload;
  },
};

const actions = {
  GET_ORDERS: async (context, payload) => {
    const currentPage = payload.page || 1;
    const refreshList = payload.refresh || false;
    const prepend = payload.prepend || false;

    context.commit("SET_AWAITRESPONSE", true);

    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/request?page=${currentPage}&sort=-id`,
      method: "get",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((response) => {
        const data = response.data;
        const parameters = {
          data,
          prepend,
          size: response.headers["x-pagination-page-count"],
          page: response.headers["x-pagination-current-page"],
        };

        if (response.status == 200) {
          if (!refreshList) {
            if (
              currentPage != state.orders.page &&
              (currentPage != 1 || prepend)
            ) {
              context.commit("SET_ORDERS", parameters);
            } else if (currentPage == 1 && !prepend) {
              context.commit("REFRESH_ORDERS", parameters);
            }
          } else {
            context.commit("REFRESH_ORDERS", parameters);
          }

          context.commit("SET_AWAITRESPONSE", false);
        }
      })
      .catch((err) => {
        context.commit("SET_AWAITRESPONSE", false);
        error.errorResponse(err.response, context.dispatch);
      });
  },

  GET_ORDER: async (context, payload) => {
    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/request/${payload}`,
      method: "get",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
      },
    })
      .then((response) => {
        const data = response.data;

        if (response.status == 200) {
          context.commit("SET_ORDER", data);
        }
      })
      .catch((err) => {
        error.errorResponse(err.response, context.dispatch);
      });
  },

  PUSH_ORDER: async (context, payload) => {
    context.commit("SET_ORDERSTATUS", {
      response: false,
      success: null,
      data: null,
    });
    context.commit("SET_AWAITRESPONSE", true);

    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/request/register`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
        "Content-Type": "application/json",
      },
      data: payload,
    })
      .then((response) => {
        const data = response.data;

        context.commit("SET_AWAITRESPONSE", false);
        if (response.status == 200) {
          context.commit("SET_ORDERSTATUS", {
            response: true,
            success: data.data ? data.success : true,
            data: data.data,
          });
        }
      })
      .catch((err) => {
        context.commit("SET_AWAITRESPONSE", false);
        error.errorResponse(err.response, context.dispatch);
        context.commit("SET_ORDERSTATUS", {
          response: true,
          success: false,
          data: state.order,
        });
      });
  },

  SET_ORDER: (context, payload) => {
    context.commit("SET_ORDER", payload);
  },

  PUSH_VOTE: async (context, payload) => {
    await Axios({
      url: `/api/${process.env.VUE_APP_API_VERSION}/request/vote/${payload.id}`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${context.getters.TOKEN}`,
        "Content-Type": "application/json",
      },
      data: payload.data,
    })
      .then((response) => {
        const data = response.data;
        context.commit("SET_AWAITRESPONSE", false);

        if (response.status == 200) {
          context.commit("SET_VOTE", data.vote);
          context.dispatch("SHOW_ALERT", {
            message: "Спасибо за вашу оценку",
          });
        }
      })
      .catch((err) => {
        context.commit("SET_AWAITRESPONSE", false);
        error.errorResponse(err.response, context.dispatch);

        if (err.response.status == 406) {
          context.dispatch("SHOW_ALERT", {
            message: "Вам запрещено голосовать",
          });
        }
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
