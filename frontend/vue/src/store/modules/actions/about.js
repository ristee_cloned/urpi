import Axios from "axios";
import error from "../error";

const state = {
    about: null
};

const getters = {
    ABOUT: state => state.about
};

const mutations = {
    SET_ABOUT: (state, payload) => {
        state.about = payload;
    }
};

const actions = {
    GET_ABOUT: async context => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/about`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_ABOUT", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
