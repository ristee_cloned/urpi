import Axios from "axios";
import error from "../error";

const state = {
    statuses: null,
    statusesInvoices: null
};

const getters = {
    STATUSES: state => state.statuses,
    STATUSESINVOICES: state => state.statusesInvoices
};

const mutations = {
    SET_STATUSES: (state, payload) => {
        state.statuses = payload;
    },

    SET_STATUSESINVOICES: (state, payload) => {
        state.statusesInvoices = payload;
    }
};

const actions = {
    GET_STATUSES: async context => {
        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/request/statuses`,
                method: "get",
                headers: {
                    Authorization: `Bearer ${context.getters.TOKEN}`
                }
            })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    localStorage.setItem("statuses", JSON.stringify(data));
                    context.commit("SET_STATUSES", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    GET_STATUSESINVOICES: async context => {
        await Axios({
                url: `/api/${process.env.VUE_APP_API_VERSION}/payment/statuses`,
                method: "get",
                headers: {
                    Authorization: `Bearer ${context.getters.TOKEN}`
                }
            })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    localStorage.setItem("statusesInvoices", JSON.stringify(data));
                    context.commit("SET_STATUSESINVOICES", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};