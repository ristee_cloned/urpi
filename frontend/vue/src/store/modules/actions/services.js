import Axios from "axios";
import error from "../error";

const state = {
    servicesList: null,
    servicesCategory: new Array(),
    servicesItem: null
};

const getters = {
    SERVICESLIST: state => state.servicesList,

    SERVICESCATEGORY: state => state.servicesCategory,

    SERVICESITEM: state => state.servicesItem
};

const mutations = {
    SET_SERVICESLIST: (state, payload) => {
        state.servicesList = payload;
    },

    SET_SERVICESCATEGORY: (state, payload) => {
        const list = state.servicesCategory.filter(
            item => item.id != payload.id
        );

        list.push({
            id: payload.id,
            services: payload.services
        });

        state.servicesCategory = list;
    },

    SET_SERVICESITEM: (state, payload) => {
        state.servicesItem = payload;
    }
};

const actions = {
    GET_SERVICESLIST: async context => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/service-category`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;
                if (response.status == 200) {
                    context.commit("SET_SERVICESLIST", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    GET_SERVICESCATEGORY: async (context, payload) => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/service-category/${payload}`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_SERVICESCATEGORY", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    },

    SET_SERVICESITEM: (context, payload) => {
        context.commit("SET_SERVICESITEM", payload);
    },

    GET_SERVICESITEM: async (context, payload) => {
        await Axios({
            url: `/api/${process.env.VUE_APP_API_VERSION}/service/${payload}`,
            method: "get",
            headers: {
                Authorization: `Bearer ${context.getters.TOKEN}`
            }
        })
            .then(response => {
                const data = response.data;

                if (response.status == 200) {
                    context.commit("SET_SERVICESITEM", data);
                }
            })
            .catch(err => {
                error.errorResponse(err.response, context.dispatch);
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
