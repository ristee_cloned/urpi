import VueCookies from "vue-cookies";
import Router from "@/router/index.js";

const errorResponse = (response, dispatch) => {
    if (response.status == 401) {
        localStorage.clear();
        VueCookies.remove("token");
        Router.push({
            name: "Welcome"
        });
        dispatch("SHOW_ALERT", {
            message: "Пожалуйста, авторизуйтесь заново"
        });
    }

    if (
        response.status == 500 ||
        response.status == 522 ||
        response.status == 524 ||
        response.status == 503 ||
        response.status == 504
    ) {
        dispatch("SHOW_ALERT", {
            message: "Что-то пошло не так, попробуйте позже"
        });
    }

    if (response.status == 422) {
        dispatch("SHOW_ALERT", {
            message: response.data[0].message
        });
    }
};

export default {
    errorResponse
};