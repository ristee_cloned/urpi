import VueCookies from "vue-cookies";

const state = {
    token: VueCookies.get("token") ? VueCookies.get("token") : null,
    awaitResponse: false
};

const getters = {
    TOKEN: state => state.token,

    AWAITRESPONSE: state => state.awaitResponse
};

const mutations = {
    SET_TOKEN: (state, payload) => {
        state.token = payload;
    },

    SET_AWAITRESPONSE: (state, payload) => {
        state.awaitResponse = payload;
    }
};

const actions = {
    SET_TOKEN: (context, payload) => {
        VueCookies.set("token", payload, null, null, null, null, "Lax");
        context.commit("SET_TOKEN", payload);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
