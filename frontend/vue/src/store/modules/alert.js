const state = {
    message: null,
    type: ''
};

const getters = {
    MESSAGE: state => state,
}

const mutations = {
    SHOW_ALERT: (state, payload) => {
        state.message = payload.message;
        state.type = payload.type
    },
};

const actions = {
    SHOW_ALERT: (context, payload) => {

        context.commit('SHOW_ALERT', payload)

        setTimeout(() => {
            context.commit('SHOW_ALERT', {
                message: null,
                type: ''
            })
        }, 5000)
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};