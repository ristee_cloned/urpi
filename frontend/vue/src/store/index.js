import Vue from "vue";
import Vuex from "vuex";

import alert from "./modules/alert";
import actions from "./modules/actions";

import news from "./modules/actions/news";
import projects from "./modules/actions/projects";
import about from "./modules/actions/about";
import feedback from "./modules/actions/feedback";
import user from "./modules/actions/user";

import services from "./modules/actions/services";
import orders from "./modules/actions/orders";

import invoices from "./modules/actions/invoices";

import statuses from "./modules/actions/statuses";

import resendPassword from "./modules/actions/resendPassword";

import pushToken from "./modules/actions/pushToken";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        alert,
        actions,

        news,
        projects,
        about,
        feedback,
        user,

        services,
        orders,

        invoices,

        statuses,

        resendPassword,

        pushToken
    }
});