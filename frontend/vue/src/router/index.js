import Vue from "vue";
import VueRouter from "vue-router";

// Авторизация
import Welcome from "@/views/Welcome.vue";
import Login from "@/views/Authorizing/Login.vue";
import ChooseRegistration from "@/views/Authorizing/ChooseRegistration.vue";
import RegistrationStepOne from "@/views/Authorizing/RegistrationStepOne.vue";
import RegistrationStepTwo from "@/views/Authorizing/RegistrationStepTwo.vue";
import PasswordResend from "@/views/Authorizing/PasswordResend.vue";
import MailConfirm from "@/views/Authorizing/MailConfirm.vue";

// Общие страницы
import NewsList from "@/views/Common/News/NewsList.vue";
import News from "@/views/Common/News/News.vue";

import PortfolioList from "@/views/Common/Portfolio/PortfolioList.vue";
import Portfolio from "@/views/Common/Portfolio/Portfolio.vue";

import About from "@/views/Common/About.vue";
import Contacts from "@/views/Common/Contacts.vue";
import Feedback from "@/views/Common/Feedback.vue";
import FeedbackMessage from "@/views/Common/FeedbackMessage.vue";

import Agreement from "@/views/Common/Agreement.vue";

// Корпоративный пользователь
import CorporateMain from "@/views/Corporate/Main.vue";
import CorporateHome from "@/views/Corporate/Home.vue";
import CorporateProfile from "@/views/Corporate/Profile.vue";

import CorporateOrderCreate from "@/views/Corporate/Order/OrderCreate.vue";
import CorporateOrderCompleted from "@/views/Corporate/Order/OrderCompleted.vue";
import CorporateOrderHistory from "@/views/Corporate/Order/OrderHistory.vue";
import CorporateOrder from "@/views/Corporate/Order/Order.vue";

// Розничный пользователь
import RetailMain from "@/views/Retail/Main.vue";
import RetailProfile from "@/views/Retail/Profile.vue";

import RetailServicesList from "@/views/Retail/Services/List.vue";
import RetailService from "@/views/Retail/Services/Service.vue";
import RetailServiceItem from "@/views/Retail/Services/ServiceItem.vue";

import RetailOrderHistory from "@/views/Retail/Order/History.vue";
import RetailOrder from "@/views/Retail/Order/Order.vue";
import RetailShoppingCart from "@/views/Retail/Order/ShoppingCart.vue";
import OrderCompleted from "@/views/Retail/Order/OrderCompleted.vue";

import RetailInvoiceList from "@/views/Retail/Invoice/InvoiceList.vue";
import RetailInvoice from "@/views/Retail/Invoice/Invoice.vue";
import RetailInvoiceResult from "@/views/Retail/Invoice/InvoiceResult.vue";
import RetailInvoiceSuccess from "@/views/Retail/Invoice/InvoiceSuccess.vue";
import RetailInvoiceFail from "@/views/Retail/Invoice/InvoiceFail.vue";

// Страница для редиректа к заказу из пуша
import PushRouterPage from "@/views/Common/PushRouterPage.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "Welcome",
        component: Welcome,
        meta: {
            guest: true
        }
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
        path: "/choose-registration",
        name: "ChooseRegistration",
        component: ChooseRegistration
    },
    {
        path: "/registration-step-one",
        name: "RegistrationStepOne",
        component: RegistrationStepOne,
        props: true
    },
    {
        path: "/registration-step-two",
        name: "RegistrationStepTwo",
        component: RegistrationStepTwo,
        props: true
    },
    {
        path: "/resend-password",
        name: "ResendPassword",
        component: PasswordResend
    },
    {
        path: "/registration/completed",
        name: "MailConfirm",
        component: MailConfirm
    },
    {
        path: "/agreement",
        name: "Agreement",
        component: Agreement
    },
    {
        path: '/order/:id',
        name: 'PushRouterPage',
        component: PushRouterPage,
        props: true
    },
    {
        path: "*",
        redirect: Welcome
    },
    {
        path: "/corporate",
        component: CorporateMain,
        meta: {
            requiresAuth: true,
            is_corporate: true
        },
        children: [{
                path: "",
                name: "CorporateHome",
                component: CorporateHome
            },
            {
                path: "news",
                name: "NewsList",
                component: NewsList,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "news/:id",
                name: "News",
                component: News,
                props: true
            },
            {
                path: "portfolio",
                name: "CorporatePortfolioList",
                component: PortfolioList
            },
            {
                path: "portfolio/:id",
                name: "CorporatePortfolio",
                component: Portfolio,
                props: true
            },
            {
                path: "about",
                name: "About",
                component: About
            },
            {
                path: "contacts",
                name: "Contacts",
                component: Contacts
            },
            {
                path: "profile",
                name: "CorporateProfile",
                component: CorporateProfile
            },
            {
                path: "feedback",
                name: "CorporateFeedback",
                component: Feedback
            },
            {
                path: "feedback-done",
                name: "CorporateFeedbackMessage",
                component: FeedbackMessage
            },
            {
                path: "order/create",
                name: "CorporateOrderCreate",
                component: CorporateOrderCreate
            },
            {
                path: "order/history",
                name: "CorporateOrderHistory",
                component: CorporateOrderHistory
            },
            {
                path: "order/completed",
                name: "CorporateOrderCompleted",
                component: CorporateOrderCompleted
            },
            {
                path: "order/history/:id",
                name: "CorporateOrder",
                component: CorporateOrder,
                props: true
            },
            {
                path: "*",
                redirect: CorporateHome
            }
        ]
    },
    {
        path: "/retail",
        redirect: RetailServicesList,
        component: RetailMain,
        meta: {
            requiresAuth: true,
            is_retail: true
        },
        children: [{
                path: "services",
                name: "RetailServicesList",
                component: RetailServicesList
            },
            {
                path: "services/:id",
                name: "RetailService",
                component: RetailService,
                props: true
            },
            {
                path: "services/:id/:service",
                name: "RetailServiceItem",
                component: RetailServiceItem,
                props: true
            },
            {
                path: "order/history/",
                name: "RetailOrderHistory",
                component: RetailOrderHistory,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "order/history/:id",
                name: "RetailOrder",
                component: RetailOrder,
                props: true
            },
            {
                path: "order/shopping-cart",
                name: "RetailShoppingCart",
                component: RetailShoppingCart
            },
            {
                path: "order/order-completed",
                name: "RetailOrderCompleted",
                component: OrderCompleted
            },
            {
                path: "payment",
                name: "RetailInvoiceList",
                component: RetailInvoiceList,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "payment/pay/:id",
                name: "RetailInvoice",
                component: RetailInvoice,
                props: true
            },
            {
                path: "payment/result",
                name: "RetailInvoiceResult",
                component: RetailInvoiceResult,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "payment/success",
                name: "RetailInvoiceSuccess",
                component: RetailInvoiceSuccess,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "payment/fail",
                name: "RetailInvoiceFail",
                component: RetailInvoiceFail,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "profile",
                name: "RetailProfile",
                component: RetailProfile
            },
            {
                path: "news",
                name: "RetailNewsList",
                component: NewsList,
                props: route => ({
                    query: route.query.q
                })
            },
            {
                path: "news/:id",
                name: "RetailNews",
                component: News,
                props: true
            },
            {
                path: "portfolio",
                name: "RetailPortfolioList",
                component: PortfolioList
            },
            {
                path: "portfolio/:id",
                name: "RetailPortfolio",
                component: Portfolio,
                props: true
            },
            {
                path: "about",
                name: "RetailAbout",
                component: About
            },
            {
                path: "contacts",
                name: "RetailContacts",
                component: Contacts
            },
            {
                path: "feedback",
                name: "RetailFeedback",
                component: Feedback
            },
            {
                path: "feedback-done",
                name: "RetailFeedbackMessage",
                component: FeedbackMessage
            },
            {
                path: "*",
                redirect: RetailServicesList
            }
        ]
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            setTimeout(() => {
                document.documentElement.scrollTop = savedPosition.y;

                return savedPosition;
            }, 600);
        } else {
            // return {
            //     x: 0,
            //     y: 0
            // };
        }
    }
});

router.beforeEach((to, from, next) => {
    let token = window.$cookies.get("token");

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (token == null) {
            next({
                name: "Welcome"
            });

            localStorage.clear();
        } else {
            let user = localStorage.getItem("user");

            if (user == "retail") {
                if (to.matched.some(record => record.meta.is_retail)) {
                    next();
                } else {
                    next({
                        name: "RetailServicesList"
                    });
                }
            } else if (user == "corporate") {
                if (to.matched.some(record => record.meta.is_corporate)) {
                    next();
                } else {
                    next({
                        name: "CorporateHome"
                    });
                }
            } else if (user != "corporate" || user != "retail") {
                localStorage.removeItem("user");
                window.$cookies.remove("token");

                next({
                    name: "Welcome"
                });
            }
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (token == null) {
            next();
        } else {
            const user = localStorage.getItem("user");

            if (user == "retail") {
                next({
                    name: "RetailServicesList"
                });
            } else if (user == "corporate") {
                next({
                    name: "CorporateHome"
                });
            } else if (user != "corporate" || user != "retail") {
                localStorage.removeItem("user");
                window.$cookies.remove("token");

                next({
                    name: "Welcome"
                });
            }
        }
    } else {
        next();
    }
});

export default router;