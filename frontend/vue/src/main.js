import Vue from "vue";
import App from "./App.vue";
import store from './store'
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import Vuelidate from "vuelidate";
import VueTheMask from "vue-the-mask";
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
Vue.use(Vuelidate);
Vue.use(VueTheMask);
Vue.use(VueCookies)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");