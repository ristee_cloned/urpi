const path = require("path");

module.exports = {

    devServer: {
        proxy: {
            '/api': {
                target: 'http://urpi.loc'
            }
        }
    },

    outputDir: '../web',

    chainWebpack: config => {
        const types = ["vue-modules", "vue", "normal-modules", "normal"];
        types.forEach(type =>
            addStyleResource(config.module.rule("less").oneOf(type))
        );
    },

    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'less',
            patterns: []
        }
    }
};

function addStyleResource(rule) {
    rule.use("style-resource")
        .loader("style-resources-loader")
        .options({
            patterns: [path.resolve(__dirname, "./src/styles/style.less")]
        });
}