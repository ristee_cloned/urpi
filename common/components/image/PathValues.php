<?php

namespace common\components\image;

/**
 * Class PathValues
 * @package common\components\image
 */
class PathValues
{
    private $sourcePath;
    private $destinationPath;

    /**
     * PathValues constructor.
     * @param string $sourcePath
     * @param string $destinationPath
     */
    public function __construct(string $sourcePath, string $destinationPath)
    {
        $this->sourcePath = $sourcePath;
        $this->destinationPath = $destinationPath;
    }

    /**
     * Gets the initial path to the resource
     */
    public function getSourcePath(): string
    {
        return $this->sourcePath;
    }

    /**
     * Gets the destination path to the resource
     */
    public function getDestinationPath(): string
    {
        return $this->destinationPath;
    }
}
