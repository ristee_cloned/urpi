<?php

namespace common\components;

use common\models\RequestDetailsRegular;

/**
 * Class ResultTotal
 * @package common\components
 */
class ResultTotal
{
    /**
     * @param  RequestDetailsRegular[] $models
     * @return integer $total
     */
    public static function countAllServices($models)
    {
        $total = 0;

        foreach ($models as $model) {
            $total += self::countServiceItem($model);
        }

        return $total;
    }

    /**
     * @param  RequestDetailsRegular $model
     * @return integer
     */
    public static function countServiceItem($model)
    {
        return $model->quantity * $model->price;
    }

    /**
     * @param RequestDetailsRegular[] $models
     * @param integer $discount
     * @return float
     */
    public static function countAllServicesWithDiscount($models, $discount)
    {
        $total = self::countAllServices($models);

        return self::calculateDiscount($total, $discount);
    }

    public static function calculateDiscount($amount, $discount)
    {
        $ratio = 1;

        if ($discount >= 0 && $discount < 100) {
            $ratio = (100 - $discount) / 100;
        }

        return round($amount * $ratio, 0, PHP_ROUND_HALF_DOWN);
    }
}
