<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'urpi.club@gmail.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.imageWidth' => 1200,
    'user.imageHeight' => 600,
    'user.imageRelPath' => '/upload/images/user/',
    'post.imageWidth' => 1200, // width image for resize
    'post.imageHeight' => 600, // height image for resize
    'post.imageRelPath' => '/upload/images/posts/', // posts image relative path
    'about.imageWidth' => 1000,
    'about.imageHeight' => 550,
    'about.imageRelPath' => '/upload/images/about/',
    'service.importFilePath' => '/runtime/upload/catalog.xlsx',
    'service.imageRelPath' => '/upload/images/service/',
    'service.imageWidth' => 600,
    'service.imageHeight' => 400,
    'category.imageRelPath' => '/upload/images/service-category/',
    'category.imageWidth' => 150,
    'category.imageHeight' => 150,
    'portfolio.imageRelPath' => '/upload/images/project/',
    'portfolio.imageWidth' => 600,
    'portfolio.imageHeight' => 400,
    'request.imageRelPath' => '/upload/images/request/',
    'request.imagesUploadCount' => 4,
    'default.imageEmpty' => '/image/image_icon.png',
    'token.expireTime' => 30, // days after token expired
    'upload.imageTypes' => ['jpeg', 'jpg', 'svg', 'png'],
    'user.discount' => 5, //percents
];
