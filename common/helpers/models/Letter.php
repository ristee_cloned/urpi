<?php

namespace common\helpers\models;

use yii\base\Model;

/**
 * Class Letter
 *
 * @package common\helpers\models
 */
class Letter extends Model
{
    public $from;
    public $to;
    public $subject;
    public $body;
}