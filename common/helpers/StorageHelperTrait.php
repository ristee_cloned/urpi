<?php


namespace common\helpers;


/**
 * Trait StorageHelperTrait
 *
 * @package common\models
 */
trait StorageHelperTrait
{
    /**
     * @param string $field
     * @return array|string
     */
    public function getImage($field = 'image')
    {
        if (is_array($this->$field)) {
            $ret = [];
            foreach ($this->$field as $item) {
                $ret[] = StorageHelper::getWebPath($item);
            }

            return $ret;
        }

        return StorageHelper::getWebPath($this->$field);
    }
}
