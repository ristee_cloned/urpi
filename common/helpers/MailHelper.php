<?php


namespace common\helpers;


use common\helpers\models\Letter;

class MailHelper
{
    public static function sendToSupport(Letter $letter)
    {
        $senderMail = \Yii::$app->params['senderEmail'];
        $supportMail = \Yii::$app->params['supportEmail'];

        $htmlBody = nl2br("$letter->body");

        $mail = \Yii::$app->mailer->compose()
            ->setFrom($senderMail)
            ->setTo($supportMail)
            ->setSubject($letter->subject)
            ->setTextBody($letter->body)
            ->setHtmlBody($htmlBody);

         if ($mail->send()) {
             return true;
         };

        return false;
    }
}
