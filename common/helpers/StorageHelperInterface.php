<?php


namespace common\helpers;


interface StorageHelperInterface
{
    public function getImage();
}