<?php


namespace common\helpers;


use Yii;
use yii\helpers\Url;

class StorageHelper
{
    const STORAGE_ALIAS = '@storage';
    
    public static function getRootPath($path)
    {
        return Yii::getAlias(self::STORAGE_ALIAS . $path);
    }

    public static function getWebPath($path)
    {
        $storageUrl = Yii::$app->params['storageBaseUrl'];
        $fullPath = self::getRootPath($path);
        $defaultImage = Yii::$app->params['default.imageEmpty'];

        if (empty($path) || !file_exists($fullPath)) {
            return Url::to($storageUrl.$defaultImage);
        }

        return Url::to($storageUrl.$path);
    }
}