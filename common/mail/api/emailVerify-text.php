<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(
    [
        $this->params['url'],
        'token' => $user->verification_token,
    ]
);
?>
Здравствуйте, <?= $user->email ?>,

Перейдите по ссылке ниже, чтобы подтвердить свою электронную почту:

<?= $verifyLink ?>
