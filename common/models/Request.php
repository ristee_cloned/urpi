<?php

namespace common\models;

use common\components\ResultTotal;
use common\helpers\StorageHelper;
use Swagger\Annotations as SWG;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use DateTime;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $appointed_at
 * @property int $type
 * @property int $status
 * @property int|null $status_updated_at
 * @property string|null $executor
 * @property int|null $vote
 * @property int $total
 * @property string $images
 * @property string $comment
 *
 * @property bool $isCorporate
 *
 * @property int $discount
 *
 * @property Payment $payment
 * @property User $user
 * @property RequestDetailsCorporate $requestDetailsCorporate
 * @property RequestDetailsRegular[] $requestDetailsRegular
 * @property Cart $cart
 * @property Service[] $services
 *
 * @SWG\Definition(definition="Request", type="object",
 *     required={"type", "user_id"},
 *     @SWG\Property(property="id", type="integer", example=23),
 *     @SWG\Property(property="appointed_at", type="string", example=2),
 *     @SWG\Property(property="status", description="Статус", type="integer", example=1, externalDocs="/v1/request/statuses"),
 *     @SWG\Property(property="status_updated_at", description="Время обновления статуса", type="string", example="15/03/20 11:03"),
 *     @SWG\Property(property="executor", description="Исполнитель", type="string", example="Иванов Иван Иванович"),
 *     @SWG\Property(property="vote", description="Оценка", type="integer", example="1"),
 *     @SWG\Property(property="total", description="Итого", type="integer", example="21340"),
 *     @SWG\Property(property="comment", description="Комментарий", type="string", example="Текст комментария"),
 *     @SWG\Property(property="images", description="Изображения", type="array",
 *        example={"//example.com/upload/images/request/SPLB6Suees4f1lJIIjx6BRFRac6isDyi.png","//example.com/upload/images/request/SPLB6Suees4f1lJIIjx6BRFRac6isDyi.png"},
 *        @SWG\Items(type="string")
 *     ),
 *     @SWG\Property(property="services", description="Услуги в заявке", type="array",
 *        @SWG\Items(ref="#/definitions/RequestDetailsRegular"),
 *     ),
 *     @SWG\Property(type="object", property="_links",
 *         @SWG\Property(type="object", property="self",
 *                @SWG\Property(type="string", property="href", example="/v1/request/23"),
 *         )
 *     )
 * )
 */
class Request extends ActiveRecord implements Linkable
{
    const STATUS_NEW = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_IN_WORK = 3;
    const STATUS_PAID = 4;
    const STATUS_CLOSED = 5;
    const STATUS_REJECTED = 6;

    const TYPE_REGULAR = 0;
    const TYPE_CORPORATE = 1;

    const SCENARIO_VOTE = 'vote';

    const VOTE_POSITIVE = 1;
    const VOTE_NEGATIVE = -1;
    const VOTE_NEUTRAL = 0;

    public $appointedAt;
    public $statusOld;
    public $executionTime;
    public $services;
    private $cart;
    private $discount = 0;

    /**
     * Request constructor.
     * @param Cart|null $cart
     * @param array $config
     */
    public function __construct(Cart $cart = null, array $config = [])
    {
        $this->cart = $cart;

        if ($cart && $this->cart->user) {
            $user = $this->cart->user;
            $this->type = $user->type;
            $this->user_id = $user->id;
            switch ($this->type) {
                case User::TYPE_REGULAR:
                    $this->total = $this->cart->getServicesTotal();
                    $this->services = $this->cart->services;
                    $this->discount = \Yii::$app->params['user.discount'];
                    break;
            }
            $this->images = Json::encode($this->cart->images);
            $this->comment = $this->cart->comment;
        }

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'appointed_at',
                    'type',
                    'status',
                    'status_updated_at',
                    'vote'
                ],
                'integer',
            ],
            [
                ['appointedAt'],
                'datetime',
                'format' => 'php:d.m.Y H:i',
                'timestampAttribute' => 'appointed_at',
            ],
            [
                'status',
                'default',
                'value' => self::STATUS_NEW,
            ],
            [
                'status',
                'in',
                'range' => [
                    self::STATUS_NEW,
                    self::STATUS_CONFIRMED,
                    self::STATUS_IN_WORK,
                    self::STATUS_PAID,
                    self::STATUS_CLOSED,
                    self::STATUS_REJECTED,
                ],
            ],
            [['type'], 'required'],
            [['executor'], 'trim'],
            [['executor'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
            ['user_id', 'required'],
            ['total', 'integer'],
            [['comment'], 'string', 'max' => 1000],
            [['comment'], 'trim'],
            [['vote'], 'default', 'value' => self::VOTE_NEUTRAL],
            [['vote'], 'required', 'on' => self::SCENARIO_VOTE],
            [['vote'], 'in', 'range' => [self::VOTE_NEGATIVE, self::VOTE_POSITIVE], 'on' => self::SCENARIO_VOTE],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'appointedAt' => 'Дата и время',
            'type' => 'Тип',
            'status' => 'Статус',
            'status_updated_at' => 'Обновление статуса',
            'executor' => 'Исполнитель',
            'vote' => 'Оценка',
            'images' => 'Изображения',
            'comment' => 'Описание',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_VOTE] = ['vote'];

        return $scenarios;
    }


    public function fields()
    {
        $fields = parent::fields();

        $fields['services'] = function () {
            return $this->getServices();
        };

        $fields['created_at'] = function() {
            return \Yii::$app->formatter->asDate($this->created_at, 'php:j/m/y');
        };

        $fields['status_updated_at'] = function() {
            if (empty($this->status_updated_at)) {
                return null;
            }

            return \Yii::$app->formatter->asDate($this->status_updated_at, 'php:j/m/y H:m');
        };
        $fields['appointed_at'] = function () {
            if (empty($this->appointed_at)) {
                return null;
            }

            return \Yii::$app->formatter->asDate($this->appointed_at, 'php:j/m/y');
        };

        $fields['images'] = function () {
            return $this->getImages(true);
        };
        $fields['vote'] = function () {
            return (int)$this->vote;
        };

        $fields['execution_time'] = function() {
            return $this->executionTime;
        };

        unset($fields['updated_at'], $fields['user_id'], $fields['type']);

        return $fields;
    }


    /**
     * If appointed_at is set, it will format after find model.
     * The old status value will be assigned to the  model property.
     * The request execution time will be set.
     *
     * @throws \Exception
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->setAppointedAt();
        $this->setStatusOld();
        $this->setExecutionTime();
        $this->setServices();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        $this->setStatusUpdateAt();
        $this->updateTotal();

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            if ($this->user->type == User::TYPE_REGULAR) {
                $this->saveRequestServices();
            }
        }

        $this->sendChangeStatusPush();

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @return ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRequestDetailsCorporate()
    {
        return $this->hasOne(
            RequestDetailsCorporate::class,
            ['id' => 'id']
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getRequestDetailsRegular()
    {
        return $this->hasMany(
            RequestDetailsRegular::class,
            ['request_id' => 'id']
        );
    }

    /**
     * @return ActiveQuery
     */
    public static function getAllRegular()
    {
        return self::find()
            ->with('user')
            ->where(['type' => self::TYPE_REGULAR])
            ->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return ActiveQuery
     */
    public static function getAllCorporate()
    {
        return self::find()
            ->with('user')
            ->where(['type' => self::TYPE_CORPORATE])
            ->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return bool
     */
    public function getIsNew()
    {
        if (self::STATUS_NEW == $this->status) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getIsClosed()
    {
        if (self::STATUS_CLOSED == $this->status) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getIsCorporate()
    {
        if (self::TYPE_CORPORATE == $this->type) {
            return true;
        }
        return false;
    }

    /**
     * @return Cart|null
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @return float|int
     */
    public function calculateTotalCost()
    {
        $total = 0;

        if (!$this->getIsCorporate()) {
            $services = $this->services;

            if ($this->user->hasDiscount()) {
                $total = ResultTotal::countAllServicesWithDiscount(
                    $services,
                    $this->discount
                );
            } else {
                $total = ResultTotal::countAllServices($services);
            }
        }

        return $total;
    }

    public static function getStatuses()
    {
        return [
            Request::STATUS_NEW => 'Новая',
            Request::STATUS_CONFIRMED => 'Подтверждена',
            Request::STATUS_IN_WORK => 'В работе',
            Request::STATUS_PAID => 'Оплачена',
            Request::STATUS_CLOSED => 'Закрыта',
            Request::STATUS_REJECTED => 'Отклонена',
        ];
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatuses();

        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $this->status;
    }

    /**
     * Sets the execution time of the request
     *
     * @throws \Exception
     */
    protected function setExecutionTime()
    {
        if (self::STATUS_CLOSED == $this->status) {
            $interval = $this->getDateTimeInterval(
                $this->status_updated_at,
                $this->created_at
            );
            $this->executionTime = $interval;
        }
    }

    /**
     * Returns the interval between two timestamps or empty string
     *
     * @param integer $timestamp1 timestamp
     * @param integer $timestamp2 timestamp
     * @return string
     * @throws \Exception
     */
    protected function getDateTimeInterval($timestamp1, $timestamp2)
    {
        $format = 'd.m.Y H:i';

        if (($dateTime1 = date($format, $timestamp1)) &&
            ($dateTime2 = date($format, $timestamp2))
        ) {
            $dateTime1 = new DateTime($dateTime1);
            $dateTime2 = new DateTime($dateTime2);
            $interval = $dateTime2->diff($dateTime1);
            return $interval->format('%d д %h ч %i м');
        }

        return '';
    }

    /**
     * Updates status change time
     */
    protected function setStatusUpdateAt()
    {
        if (self::STATUS_CLOSED != $this->statusOld &&
            $this->status != $this->statusOld
        ) {
            $this->status_updated_at = time();
        }
    }

    /**
     * Sets the services of the request
     */
    protected function setServices()
    {
        if ($this->getIsCorporate()) {
            $this->services = $this->requestDetailsCorporate;
        } else {
            $this->services = $this->requestDetailsRegular;
        }
    }

    public function getServices()
    {
        if ($this->services && is_array($this->services)) {
            foreach ($this->services as $service) {
                $service->image = StorageHelper::getWebPath($service->image);
            }
        }

        return $this->services;
    }

    /**
     * Formats the timestamp for display
     */
    protected function setAppointedAt()
    {
        if ($this->appointed_at) {
            $formatDate = date('d.m.Y H:i', $this->appointed_at);
            $this->appointedAt = $formatDate ? $formatDate : '';
        }
    }

    /**
     * Remember the old request status
     */
    protected function setStatusOld()
    {
        $this->statusOld = $this->status;
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['request/view', 'id' => $this->id]),
        ];
    }

    private function saveRequestServices()
    {
        $rows = [];
        foreach ($this->services as $service) {
            $rows[] = [
                'request_id' => $this->id,
                'service_name' => $service->name,
                'quantity' => $service->quantity,
                'price' =>  $service->getPrice($this->user),
                'image' => $service->image,
            ];
        }

        $requestDetailFields = [
            'request_id',
            'service_name',
            'quantity',
            'price',
            'image',
        ];

        \Yii::$app->db->createCommand()->batchInsert(RequestDetailsRegular::tableName(), $requestDetailFields, $rows)->execute();
    }

    private function updateTotal()
    {
        $this->total = $this->calculateTotalCost();
    }

    public function setPaid()
    {
        $this->status = self::STATUS_PAID;
        $this->save();
    }

    public function isPaid()
    {
        return $this->payment && $this->payment->status == Payment::STATUS_PAID;
    }

    /**
     * @param bool $schema
     * @return array|mixed|string
     */
    public function getImages($schema = false)
    {
        if (json_decode($this->images)) {
            $this->images = Json::decode($this->images);
        }

        $images = [];
        if (is_array($this->images)) {
            if ($schema) {
                foreach ($this->images as $image) {
                    $images[] = StorageHelper::getWebPath($image);
                }

                $this->images = $images;
            }
        } else {
            $this->images = [];
        }

        return $this->images;
    }

    public function canVote()
    {
        // if ($this->type == self::TYPE_CORPORATE) {
        //     return false;
        // }

        // if (empty($this->vote)) {
        //     return true;
        // }

        return true;
    }

    private function sendChangeStatusPush()
    {
        if ($this->status == $this->statusOld) {
            return;
        }

        $user = $this->user;
        if ($user) {
            $push = new Push($user);
            $push->type = Push::PUSH_TYPE_TOKEN;
            $status = $this->getStatusLabel();

            if ($this->status == self::STATUS_CLOSED) {
                $push->title = "Заявка #{$this->id} закрыта";
                $push->body = "Работа завершена. Оцените, пожалуйста, качество работ";
            } else {
                $push->title = "Изменение статуса заявки #{$this->id}";
                $push->body = "Статус заявки #{$this->id} был изменен на $status";
            }

            $push->data['request'] = $this->id;

            $push->send();
        }
    }
}
