<?php

namespace common\models\form;

use yii;
use yii\base\Security;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use common\components\image\{
    Image,
    ImageValues,
    PathValues
};

/**
 * Class ImagesUploadForm
 * @package common\models\form
 *
 * @property int $maxFiles
 *
 */
class ImagesUploadForm extends ImageUploadForm
{
    public $maxFiles = 5;

    public function rules()
    {
        return [
            [
                ['uploadFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => $this->extensions,
                'checkExtensionByMimeType' => false,
                'maxFiles' => $this->maxFiles,
            ],
        ];
    }

    /**
     * If images have been uploaded, returns paths to images,
     * otherwise returns false
     *
     * @return bool|array
     * @throws yii\base\Exception
     */
    public function upload()
    {
        $this->basePath = Yii::getAlias('@storage');

        if ($this->validate()) {
            if ($uploadFile = $this->uploadFile) {
                $relPaths = [];
                if (!file_exists($this->basePath.$this->filePath)) {
                    FileHelper::createDirectory($this->basePath.$this->filePath);
                }
                /** @var UploadedFile $item */
                foreach ($uploadFile as $item) {
                    $security = new Security();
                    $filename = $security->generateRandomString();
                    $imageRelPath = $this->filePath . $filename.'.'.$item->extension;
                    $relPaths[] = $imageRelPath;
                    $imagePath = $this->basePath . $imageRelPath;
                    if ($item->saveAs($imagePath)) {
                        continue;
                    } else {
                        return false;
                    }
                }
                return $relPaths;
            }
            return false;
        }
        return false;
    }

    /**
     * If the images is resized returns the paths to the images,
     * otherwise returns false
     *
     * @param array $imagePaths
     * @param integer $width
     * @param integer $height
     * @return bool|array
     */
    public function resize($imagePaths, $width, $height)
    {
        $resizePaths = [];
        foreach ($imagePaths as $imagePath) {
            $pathInfo = pathinfo($imagePath);
            $fullImagePath = $this->basePath . $imagePath;
            $resizePath = $pathInfo['dirname'] . '/' .
                time() . '_' . $pathInfo['basename'];
            $fullResizePath = $this->basePath . $resizePath;
            $img = new Image(
                new PathValues($fullImagePath, $fullResizePath),
                new ImageValues($width, $height)
            );

            if ($img->resize()) {
                $this->deleteImageFile($fullImagePath);
                $resizePaths[] = $resizePath;
            }
        }

        return empty($resizePaths) ? false : $resizePaths;
    }
}
