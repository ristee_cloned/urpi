<?php

namespace common\models\form;

/**
 * Class ExcelUploadForm
 * @package common\models\form
 */
class ExcelUploadForm extends UploadForm
{
    public $extensions = 'xls, xlsx, csv';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['uploadFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => $this->extensions,
                'checkExtensionByMimeType' => false,
                'skipOnEmpty' => false,
            ],
        ];
    }
}
