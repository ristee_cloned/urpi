<?php
namespace common\models\form;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\models
 *
 * @property UploadedFile|UploadedFile[] $uploadFile
 * @property string $filePath
 * @property array $extensions
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $uploadFile;
    /**
     * @var string file upload path
     */
    public $filePath;
    /**
     * @var string|array allowed extensions
     */
    public $extensions;

    /**
     * UploadForm constructor.
     * @param string $filePath
     * @param array $config
     */
    public function __construct($filePath, array $config = [])
    {
        $this->filePath = $filePath;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['uploadFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => $this->extensions,
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'uploadFile' => 'Загрузить файл',
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $this->uploadFile->saveAs($this->filePath);
            return true;
        } else {
            return false;
        }
    }
}
