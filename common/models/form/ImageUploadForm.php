<?php

namespace common\models\form;

use yii;
use common\components\image\{
    Image,
    ImageValues,
    PathValues
};

/**
 * Class ImageUploadForm
 * @package common\models\form
 *
 * @property array $extensions
 * @property string $basePath
 */
class ImageUploadForm extends UploadForm
{
    public $extensions;
    protected $basePath;

    public function init()
    {
        $this->extensions = \Yii::$app->params['upload.imageTypes'];
        parent::init();
    }


    /**
     * If image uploaded, returns image path, otherwise returns false
     *
     * @return bool|string
     * @throws yii\base\Exception
     */
    public function upload()
    {
        $this->basePath = Yii::getAlias('@storage');

        if ($this->validate()) {
            if ($uploadFile = $this->uploadFile) {
                $folderRootPath = $this->basePath . $this->filePath;
                $relPath = $this->filePath .
                    $this->uploadFile->baseName . '.' .
                    $this->uploadFile->extension;
                $imagePath = $this->basePath . $relPath;

                if (!file_exists($folderRootPath)) {
                    yii\helpers\FileHelper::createDirectory($folderRootPath);
                }

                if ($uploadFile->saveAs($imagePath)) {
                    return $relPath;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * If the image is resized returns the path to the image,
     * otherwise returns false
     * @param string $imagePath
     * @param integer $width
     * @param integer $height
     * @return bool|string
     */
    public function resize($imagePath, $width, $height)
    {
        $pathInfo = pathinfo($imagePath);
        $fullImagePath = $this->basePath . $imagePath;
        $resizePath = $pathInfo['dirname'] . '/' .
            time() . '_' . $pathInfo['basename'];
        $fullResizePath = $this->basePath . $resizePath;
        $img = new Image(
            new PathValues($fullImagePath, $fullResizePath),
            new ImageValues($width, $height)
        );

        if ($img->resize()) {
            $this->deleteImageFile($fullImagePath);
            return $resizePath;
        }

        return false;
    }

    /**
     * Deletes the image file
     * @var string $path
     */
    public function deleteImageFile(string $path)
    {
        if (is_file($path)) {
            unlink($path);
        }
    }
}
