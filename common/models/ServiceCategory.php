<?php

namespace common\models;

use common\helpers\StorageHelperTrait;
use common\helpers\StorageHelperInterface;
use Swagger\Annotations as SWG;
use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\web\Linkable;

/**
 * This is the model class for table "service_category".
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 *
 * @property Service[] $services
 *
 * @SWG\Definition(definition="ServiceCategory", type="object",
 *     required={"name"},
 *     @SWG\Property(property="id", type="integer", example="15"),
 *     @SWG\Property(property="name", type="string", example="Категория услуги"),
 *     @SWG\Property(property="image", type="string", example="//example.com/upload/images/service-category/1583180010_filename.jpg"),
 *     @SWG\Property(type="object", property="_links",
 *         @SWG\Property(type="object", property="self",
 *                @SWG\Property(type="string", property="href", example="/v1/service-category/15"),
 *                )
 *      )
 * ),
 * @SWG\Definition(definition="ExtendedServiceCategory", type="object",
 *     allOf={
 *         @SWG\Schema(ref="#/definitions/ServiceCategory"),
 *         @SWG\Schema(
 *              @SWG\Property(property="services", type="array",
 *                  @SWG\Items(ref="#/definitions/Service"),
 *              ),
 *         )
 *     }
 * )
 */
class ServiceCategory extends ActiveRecord implements StorageHelperInterface, Linkable
{
    use StorageHelperTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'image' => 'Изображение',
        ];
    }

    public function fields()
    {
        $fields = [
            'id',
            'name',
            'image' => function (ServiceCategory $model) {
                return $model->getImage();
            },
        ];

        if (Yii::$app->controller->action->id == 'view') {
            $fields[] = 'services';
        }

        return $fields;
    }

    /**
     * @return ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(
            Service::class,
            ['service_category_id' => 'id']
        );
    }

    /**
     * @return array array of category names
     */
    public static function getAllNamesWithIds()
    {
        return self::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->orderBy('name')
            ->column();
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile($this->image);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     * @param null|string $path
     */
    public function deleteImageFile($path = null)
    {
        if (is_file($filename = Yii::getAlias('@storage' . $path))) {
            unlink($filename);
        }
    }

    /**
     * @inheritDoc
     */
    public function getLinks()
    {
        return [
            yii\web\Link::REL_SELF => Url::to(['service-category/view', 'id' => $this->id]),
        ];
    }
}
