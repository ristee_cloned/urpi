<?php

namespace common\models;

use common\helpers\StorageHelperInterface;
use common\helpers\StorageHelperTrait;
use Swagger\Annotations as SWG;
use yii;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use common\components\image\{
    Image,
    ImageValues,
    PathValues
};

/**
 * This is the model class for table "about_us".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string|null $image
 *
 * @SWG\Definition(definition="About", type="object",
 *      @SWG\Property(property="title", type="string", example="О Компании"),
 *      @SWG\Property(property="content", type="string", example="Немного текста о компании"),
 *      @SWG\Property(property="image", type="string", example="//example.com/upload/images/about/1583261557_filename.png"),
 * )
 *
 */
class About extends ActiveRecord implements StorageHelperInterface
{
    use StorageHelperTrait;

    public $imageFile;
    protected $basePath;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [
                ['imageFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => 'png, jpg, jpeg',
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['image'] = function() {
            return $this->getImage();
        };
        unset($fields['id']);

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Текст',
            'image' => 'Изображение',
            'imageFile' => 'Изображение',
        ];
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile($this->image);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     * @var string $path
     */
    public function deleteImageFile(string $path)
    {
        if (is_file($filename = Yii::getAlias('@storage' . $path))) {
            unlink($filename);
        }
    }

    /**
     * Upload image
     *
     * @return bool|string
     * @throws yii\base\Exception
     */
    public function uploadImage()
    {
        $this->basePath = Yii::getAlias('@storage');
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if ($this->imageFile) {
            $folder = Yii::$app->params['about.imageRelPath'];
            $relPath = $folder .
                $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $destPath = $this->basePath . $relPath;

            if (!file_exists($this->basePath.$folder)) {
                FileHelper::createDirectory($this->basePath.$folder);
            }

            if ($this->imageFile->saveAs($destPath)) {
                if ($this->image) {
                    $this->deleteImageFile($this->image);
                }
                return $this->image = $relPath;
            }
        }
        return false;
    }

    /**
     * Resizing image
     * @param string $relPath
     */
    public function resizeImage(string $relPath)
    {
        $thumbPath = Yii::$app->params['about.imageRelPath'] .
            time() . '_' . rand(0, 999999) . '_' .
            $this->imageFile->baseName . '.' . $this->imageFile->extension;
        $srcPath = $this->basePath . $relPath;
        $destPath = $this->basePath . $thumbPath;
        $imageWidth = Yii::$app->params['about.imageWidth'];
        $imageHeight = Yii::$app->params['about.imageHeight'];
        $image = new Image(
            new PathValues($srcPath, $destPath),
            new ImageValues($imageWidth, $imageHeight)
        );

        if ($image->resize()) {
            $this->deleteImageFile($relPath);
            $this->image = $thumbPath;
        }
    }

    /**
     * @return array|null|ActiveRecord
     */
    public static function findOneModel()
    {
        return $model = self::find()->one();
    }
}
