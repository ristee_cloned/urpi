<?php

namespace common\models;

use api\components\Robokassa;
use Swagger\Annotations as SWG;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $paid_at
 * @property int $status
 * @property int $total_price
 *
 * @property User|null $user
 * @property Request $request
 *
 * @SWG\Definition(definition="Payment", type="object",
 *     required={"total_price"},
 *     @SWG\Property(property="id", type="integer", example=15),
 *     @SWG\Property(property="total_price", type="integer", example=1000),
 *     @SWG\Property(property="created_at", description="Дата создания", type="string", example="10/02/20"),
 *     @SWG\Property(property="updated_at", description="Дата обновления", type="string", example="10/02/20"),
 *     @SWG\Property(property="paid_at", description="Дата оплаты", type="string", example="11/02/20"),
 *     @SWG\Property(property="status", description="Статус счета: 0 - не оплачен, 1 - оплачен", type="string", example="11/02/20"),
 * )
 */
class Payment extends ActiveRecord
{
    const STATUS_NOT_PAID = 0;
    const STATUS_PAID = 10;
    const STATUS_ERROR = 13;
    const STATUS_FAILED = 99;

    const SCENARIO_CHANGE_STATUS = 'change_status';

    private $req;
    public $user = null;

    /**
     * Payment constructor.
     *
     * @param Request|null $req
     * @param User|null $user
     * @param array $config
     */
    public function __construct(Request $req = null, User $user = null, array $config = [])
    {
        $this->req = $req;
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * @return Request
     */
    public function getReq()
    {
        return $this->req;
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['total'] = 'total_price';

        $fields['services'] = function() {
            return $this->request->getServices();
        };
        $fields['created_at'] = function () {
            return \Yii::$app->formatter->asDate($this->created_at, 'php:j/m/y');
        };

        $fields['updated_at'] = function () {
            return \Yii::$app->formatter->asDate($this->updated_at, 'php:j/m/y');
        };

        $fields['updated_at'] = function () {
            if (empty($this->paid_at)) {
                return null;
            }
            return \Yii::$app->formatter->asDate($this->paid_at, 'php:j/m/y H:i');
        };

        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_price'], 'required'],
            [
                [
                    'created_at',
                    'updated_at',
                    'paid_at',
                    'status',
                    'total_price',
                ],
                'integer',
            ],
            [
                'status',
                'default',
                'value' => self::STATUS_NOT_PAID,
            ],
            [
                'status',
                'in',
                'range' => [
                    self::STATUS_NOT_PAID,
                    self::STATUS_PAID,
                    self::STATUS_ERROR,
                    self::STATUS_FAILED,
                ],
            ],
            [
                ['id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Request::class,
                'targetAttribute' => ['id' => 'id'],
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_STATUS] = ['status', 'paid_at'];

        return $scenarios;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Выставлен',
            'updated_at' => 'Обновлен',
            'paid_at' => 'Оплачен',
            'status' => 'Статус',
            'total_price' => 'Сумма',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->sendPaymentCreated();
        }
    }


    /**
     * @return ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['id' => 'id']);
    }

    /**
     * @return bool
     */
    public function getIsPaid()
    {
        if (self::STATUS_PAID == $this->status) {
            return true;
        }
        return false;
    }

    /**
     * @return $this|bool|PaymentOperation|string
     * @throws ServerErrorHttpException
     */
    public function pay()
    {
        $this->updateTotalPrice();

        return $this->processPay();
    }

    public static function getStatuses($keysOnly = false)
    {
        $statuses = [
            self::STATUS_NOT_PAID => 'Не оплачено',
            self::STATUS_ERROR => 'Во время оплаты возникла ошибка',
            self::STATUS_FAILED => 'Оплата не прошла',
            self::STATUS_PAID => 'Оплачено',
        ];

        if ($keysOnly) {
            return array_keys($statuses);
        }

        return $statuses;
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatuses();
        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $this->status;
    }

    public function updateTotalPrice()
    {
        $this->refresh();
        $this->total_price = $this->request->total;

        return $this->save();
    }

    private function processPay()
    {
        $robokassa = new Robokassa();
        $robokassa->setPayment($this);

        $paymentOperation = PaymentOperation::create($robokassa);
        if ($paymentOperation->hasErrors()) {
            throw new ServerErrorHttpException('Произошла ошибка при сохранении платежа.', 500);
        }

        $robokassa->setOrderNumber($paymentOperation);

        $url = $robokassa->getUrl();
        $paymentOperation->url = $robokassa->getUrl();

        if ($paymentOperation->save()) {
            return $url;
        } else {
            return $paymentOperation;
        }
    }

    public function checkResult($postData)
    {
        $robokassa = new Robokassa();
        return $robokassa->checkResult($postData);
    }

    public function setPaid()
    {
        $this->status = self::STATUS_PAID;
        $this->paid_at = time();

        if (!$this->save()) {
            return false;
        }

        return true;
    }

    public function setFailed()
    {
        if ($this->status == self::STATUS_PAID) {
            $this->addError('status', 'Счет уже был оплачен. Невозможно изменить статус уже оплаченного счета.');

            return false;
        }
        $this->status = self::STATUS_FAILED;

        return $this->save();
    }

    public function getStatusButtonColor()
    {
        if ($this->getIsPaid()) {
            return 'success';
        } else if ($this->status == self::STATUS_FAILED || $this->status == self::STATUS_ERROR) {
            return 'warning';
        } else {
            return 'default';
        }
    }

    private function sendPaymentCreated()
    {
        $user = $this->user;
        if ($user) {
            $push = new Push($user);
            $push->type = Push::PUSH_TYPE_TOKEN;
            $request = $this->request->id;
            $push->title = "Выставлен счет";

            $push->body = "Вам был выставлен счет #{$this->id} на заявку $request. Вы можете посмотреть информацию в личном кабинете.";
            $push->data['request'] = $this->id;

            $push->send();
        }
    }
}
