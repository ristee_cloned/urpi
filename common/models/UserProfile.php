<?php

namespace common\models;

use common\helpers\StorageHelperInterface;
use common\helpers\StorageHelperTrait;
use Swagger\Annotations as SWG;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\helpers\Json;

/**
 * @SWG\Definition(definition="UserProfile", type="object",
 *     @SWG\Property(property="id", type="string", example="10"),
 *     @SWG\Property(property="name", type="string", example="Иван"),
 *     @SWG\Property(property="surname", type="string", example="Иванов"),
 *     @SWG\Property(property="phone", type="string", example="89126749522"),
 *     @SWG\Property(property="address", type="string", example="г. Екатеринбург"),
 *     @SWG\Property(property="company_name", type="string", example="ООО Металлист"),
 *     @SWG\Property(property="inn", type="string", example="6602154672"),
 *     @SWG\Property(property="discount_card", type="string", example="10"),
 * )
 *
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $phone
 * @property string|null $address
 * @property string|null $company_name
 * @property string|null $inn
 * @property string|null $discount_card
 * @property string|null $avatar
 *
 * @property User $user
 */
class UserProfile extends ActiveRecord implements StorageHelperInterface
{
    use StorageHelperTrait;

    const SCENARIO_UPDATE = 'update';

    public $_avatar;
    public $uploadImageExtensions;

    public function init()
    {
        $this->uploadImageExtensions = \Yii::$app->params['upload.imageTypes'];
        parent::init();
    }

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['avatar']);
        $fields['address'] = function() {
            return $this->getAddress();
        };

        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'phone'], 'required', 'except' => self::SCENARIO_UPDATE],
            [['name', 'surname'], 'string', 'max' => 50],
            [
                ['phone',],
                'filter',
                'filter' => [$this, 'normalizePhone'],
            ],
            [
                [
                    'address',
                    'company_name',
                    'discount_card',
                    'avatar',
                ],
                'string',
                'max' => 255,
            ],
            [
                '_avatar', 'image', 'extensions' => $this->uploadImageExtensions,
            ],
            [
                ['inn',],
                'filter',
                'filter' => [$this, 'normalizeInn'],
            ],
            [
                [
                    'name',
                    'surname',
                    'address',
                    'inn',
                    'company_name',
                    'discount_card'
                ],
                'trim'
            ],
            [
                ['name', 'surname',],
                'match',
                'pattern' => '/^[а-яёa-z]+$/iu',
                'message' => 'Пожалуйста, указывайте только буквы.'
            ],
            [
                ['name', 'surname',],
                'filter',
                'filter' => [$this, 'normalizeName'],
            ],
            [
                ['id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'company_name' => 'Название компании',
            'inn' => 'ИНН',
            'discount_card' => 'Дисконтная карта',
            'avatar' => 'Аватар',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * @param User $user
     * @return array|UserProfile|null|ActiveRecord
     */
    public static function findByUser(User $user)
    {
        $id = $user->getId();
        $profile = self::find()->where(['id' => $id])->one();

        if (is_null($profile)) {
            $profile = new self();
            $profile->id = $id;
        }

        return $profile;
    }

    /**
     * @param $value
     * @param string $encoding
     * @return bool|mixed|string
     */
    public function normalizeName($value, $encoding = 'UTF-8')
    {
        $value = mb_strtolower($value, $encoding);
        $value = mb_strtoupper(mb_substr($value, 0, 1, $encoding), $encoding) .
            mb_substr($value, 1, null, $encoding);
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function normalizePhone($value)
    {
        return preg_replace('/[\D]/', '', $value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function normalizeInn($value)
    {
        return $this->normalizePhone($value);
    }

    public function getAddress()
    {
        $addressFields = [
            'city' => 'г. ',
            'street' => 'ул. ',
            'house' => 'д. ',
            'flat' => 'кв. '
        ];

        if (json_decode($this->address)) {
            $data = Json::decode($this->address);

            $addressStr = '';
            foreach ($data as $type => $address) {
                $addressStr .= $addressFields[$type] ?? '--';
                $addressStr .= $address.', ';
            }

            $addressStr = trim($addressStr, ", ");

            return $addressStr;
        }

        return $this->address;
    }
}
