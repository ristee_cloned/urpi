<?php

namespace common\models;

use Swagger\Annotations as SWG;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "request_details_regular".
 *
 * @property int $id
 * @property int $request_id
 * @property string $service_name
 * @property int $quantity
 * @property int $price
 * @property string|null $image
 *
 * @property Request $request
 *
 * @SWG\Definition(definition="RequestDetailsRegular", type="object",
 *     required={"request_id", "service_name", "quantity", "price"},
 *     @SWG\Property(property="id", description="ID услуги в списке заявок", type="integer", example="10"),
 *     @SWG\Property(property="request_id", description="ID заявки", type="integer", example=1),
 *     @SWG\Property(property="service_name", description="Название услуги", type="string", example="Покраска стен"),
 *     @SWG\Property(property="quantity", description="Количество", type="integer", example="4"),
 *     @SWG\Property(property="price", description="Стоимость", type="integer", example="10"),
 *     @SWG\Property(property="image", description="Изображение (если услуга взята из каталога).", type="string", example="/upload/images/service/1583257628_example.png"),
 * )
 */
class RequestDetailsRegular extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_details_regular';
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'updateRequestTotal']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'updateRequestTotal']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateRequestTotal']);
        parent::init();
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'request_id',
                    'service_name',
                    'quantity',
                    'price',
                ],
                'required',
            ],
            [['request_id', 'quantity', 'price'], 'integer'],
            [['service_name'], 'trim'],
            [['service_name', 'image'], 'string', 'max' => 255],
            [
                ['request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Request::class,
                'targetAttribute' => ['request_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'Request ID',
            'service_name' => 'Наименование',
            'quantity' => 'Количество',
            'price' => 'Цена',
            'image' => 'Изображение',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['id' => 'request_id']);
    }


    public function updateRequestTotal()
    {
        $this->refresh();
        $this->request->total = $this->request->calculateTotalCost();

        $this->request->save();
    }
}
