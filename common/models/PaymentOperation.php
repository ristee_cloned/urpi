<?php

namespace common\models;

use api\components\Robokassa;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%payment_operation}}".
 *
 * @property int $id
 * @property int|null $payment_id
 * @property float|null $out_sum
 * @property string|null $signature
 * @property string|null $description
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $url
 *
 * @property Robokassa $robokassa;
 *
 * @property Payment $payment
 */
class PaymentOperation extends \yii\db\ActiveRecord
{
    const SCENARIO_CHANGE_STATUS = 'change_status';

    public $robokassa;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payment_operation}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['out_sum'], 'number'],
            [['signature'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
            [['status'], 'in', 'range' => Payment::getStatuses(true)],
        ];
    }

    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario[self::SCENARIO_CHANGE_STATUS] = ['status', 'updated_at'];

        return $scenario;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Счет',
            'out_sum' => 'Сумма',
            'signature' => 'Подпись',
            'description' => 'Описание',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * Gets query for [[Payment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @param Robokassa $robokassa
     * @return bool|PaymentOperation
     */
    public static function create(Robokassa $robokassa)
    {
        $paymentOperation = new self;
        $paymentOperation->status = Payment::STATUS_NOT_PAID;
        $paymentOperation->robokassa = $robokassa;
        $paymentOperation->payment_id = $robokassa->getPaymentId();
        $paymentOperation->out_sum = $robokassa->outSumm;
        $paymentOperation->description = $robokassa->getDescription();

        if ($paymentOperation->save()) {
            return $paymentOperation;
        }

        return $paymentOperation;
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function setFailed()
    {
        $this->refresh();
        $this->status = Payment::STATUS_FAILED;

        if (!$this->save()) {
            $errors = Json::encode($this->getErrors());
            Yii::error('PaymentOperation SetFailed: '.$errors);
        };

        $payment = $this->payment;

        if (empty($payment)) {
            throw new NotFoundHttpException('"Payment" not found for this PaymentOperation');
        }

        $payment->scenario = Payment::SCENARIO_CHANGE_STATUS;

        if (!$payment->setFailed()) {
            $paymentErrors = Json::encode($payment->getErrors());
            $paymentErrorText = "PaymentOperation: payment: setFailed: $paymentErrors";
            $this->addError('payment', 'Payment was not saved: '.$paymentErrors);
            Yii::error($paymentErrorText);

            return false;
        };

        return true;
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function setPaid()
    {
        $this->refresh();
        $this->status = Payment::STATUS_PAID;
        if (!$this->save()) {
            $errors = Json::encode($this->getErrors());
            $errorText = 'PaymentOperation setPaid: '.$errors;
            Yii::error($errorText);
        };

        $payment = $this->payment;

        if (empty($payment)) {
            throw new NotFoundHttpException('"Payment" not found for this PaymentOperation');
        }

        $payment->scenario = Payment::SCENARIO_CHANGE_STATUS;

        if (!$payment->setPaid()) {
            $errors = Json::encode($payment->getErrors());
            $errorText = 'PaymentOperation: Payment setPaid: '.$errors;
            $this->addError('payment', $errorText);
            Yii::error($errorText);

            return false;
        };

        return true;
    }

    /**
     * @param $postData
     * @return bool
     */
    public function checkResult($postData)
    {
        $password2 = Yii::$app->params['robokassa.password2'];

        if (!isset($postData['OutSum']) || isset($postData['InvId']) || isset($postData['SignatureValue'])) {
            $this->addError('id', 'Неверный ответ от платежной системы.');
            Yii::error('PaymentOperation: checkResult Неверный ответ от платежной системы.');
            return false;
        }

        $outSum = $postData['OutSum'];
        $orderNumber = $postData['InvId'];
        $signature = strtoupper($postData['SignatureValue']);

        $str = "$outSum:$orderNumber:{$password2}";
        $checkSignature = strtoupper(md5($str));

        if ($checkSignature != $signature) {
            Yii::error('PaymentOperation: checkResult: Invalid signature');
            $this->addError('signature', 'Неверная подпись');
            return false;
        }

        return true;
    }
}
