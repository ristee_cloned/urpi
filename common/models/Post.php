<?php

namespace common\models;

use common\helpers\StorageHelperInterface;
use common\helpers\StorageHelperTrait;
use Swagger\Annotations as SWG;
use yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\UploadedFile;
use common\components\image\{
    Image,
    ImageValues,
    PathValues
};

/**
 * * @SWG\Definition(definition="Post", type="object",
 *     required={"title", "content"},
 *      @SWG\Property(property="id", type="integer", example="12"),
 *      @SWG\Property(property="title", type="string", example="Новость дня",),
 *      @SWG\Property(property="content", type="string", example="Текст новости"),
 *      @SWG\Property(property="image", type="string", example="//example.com/upload/images/posts/1583180010_filename.jpg"),
 *      @SWG\Property(property="created_at", type="string", example="01/01/20"),
 *      @SWG\Property(type="object", property="_links",
 *         @SWG\Property(type="object", property="self",
 *                @SWG\Property(type="string", property="href", example="/v1/post/12"),
 *                )
 *      )
 *  )
 *
 * This is the model class for table "{{%post}}".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $image
 * @property int $created_at
 */
class Post extends ActiveRecord implements StorageHelperInterface, yii\web\Linkable
{
    use StorageHelperTrait;

    public $imageFile;
    protected $basePath;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [
                ['imageFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => 'png, jpg, jpeg',
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['image'] = function () {
            return $this->getImage();
        };
        $fields['created_at'] = function () {
            return Yii::$app->formatter->asDate($this->created_at, 'php:d/m/y');
        };

        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Текст',
            'imageFile' => 'Изображение',
            'image' => 'Изображение',
            'created_at' => 'Дата публикации',
        ];
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile($this->image);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     * @var string $path
     */
    public function deleteImageFile(string $path)
    {
        if (is_file($filename = Yii::getAlias('@storage' . $path))) {
            unlink($filename);
        }
    }

    /**
     * Upload image
     *
     * @return bool|string
     * @throws yii\base\Exception
     */
    public function uploadImage()
    {
        $this->basePath = Yii::getAlias('@storage');
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if ($this->imageFile) {
            $relFolder = Yii::$app->params['post.imageRelPath'];
            $relPath =  $relFolder .
                $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $destPath = $this->basePath . $relPath;
            $destFolder = $this->basePath . $relFolder;

            if (!file_exists($destFolder)) {
                FileHelper::createDirectory($destFolder);
            }

            if ($this->imageFile->saveAs($destPath)) {
                if ($this->image) {
                    $this->deleteImageFile($this->image);
                }
                return $this->image = $relPath;
            }
        }
        return false;
    }

    /**
     * Resizing image
     * @param string $relPath
     */
    public function resizeImage(string $relPath)
    {
        $thumbPath = Yii::$app->params['post.imageRelPath'] .
            time() . '_' . rand(0, 999999) . '_' .
            $this->imageFile->baseName . '.' . $this->imageFile->extension;
        $srcPath = $this->basePath . $relPath;
        $destPath = $this->basePath . $thumbPath;
        $imageWidth = Yii::$app->params['post.imageWidth'];
        $imageHeight = Yii::$app->params['post.imageHeight'];
        $image = new Image(
            new PathValues($srcPath, $destPath),
            new ImageValues($imageWidth, $imageHeight)
        );

        if ($image->resize()) {
            $this->deleteImageFile($relPath);
            $this->image = $thumbPath;
        }
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['post/view', 'id' => $this->id]),
        ];
    }
}
