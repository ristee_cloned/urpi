<?php

namespace common\models;

use common\helpers\StorageHelperInterface;
use common\helpers\StorageHelperTrait;
use Swagger\Annotations as SWG;
use yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property string|null $image
 *
 * @SWG\Definition(definition="Project", type="object",
 *     required={"name", "content"},
 *      @SWG\Property(property="id", type="integer", example="16"),
 *      @SWG\Property(property="title", type="string", example="Новость дня",),
 *      @SWG\Property(property="content", type="string", example="Текст новости"),
 *      @SWG\Property(property="images", type="array",
 *          @SWG\Items(
 *              type="string",
 *          ),
 *          example={"//example.com/upload/images/posts/1583180010_filename.jpg", "//example.com/upload/images/posts/1583180012_filename1.jpg"},
 *      ),
 *      @SWG\Property(type="object", property="_links",
 *         @SWG\Property(type="object", property="self",
 *                @SWG\Property(type="string", property="href", example="/v1/project/16"),
 *                )
 *      )
 *  )
 */
class Project extends ActiveRecord implements StorageHelperInterface, Linkable
{
    use StorageHelperTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['content', 'image'], 'string'],
            [['name', 'content'], 'trim'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['title'] = 'name';
        $fields['image'] = function () {
            return $this->getImage();
        };
        unset($fields['name']);

        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'content' => 'Описание',
            'image' => 'Изображения',
        ];
    }

    /**
     * After finding the image is decoded
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->image = self::jsonDecode($this->image);
    }

    /**
     * Before validating a record encode image attribute
     * @return bool
     */
    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }
        if (is_array($this->image)) {
            $this->image = self::jsonEncode($this->image);
        }
        return true;
    }

    /**
     * Before deleting a record delete images files
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (is_array($this->image)) {
                $this->deleteImagesFiles($this->image);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes image files
     * @param array $images
     */
    public function deleteImagesFiles($images)
    {
        if ($images) {
            foreach ($images as $image) {
                $this->deleteImageFile($image);
            }
        }
    }

    /**
     * Deletes the image file
     * @param null|string $path
     */
    public function deleteImageFile($path = null)
    {

        if (is_file($filename = Yii::getAlias('@storage' . $path))) {
            unlink($filename);
        }
    }

    /**
     * Returns json string
     * @param array $arrayFilePaths
     * @return string
     */
    protected static function jsonEncode($arrayFilePaths)
    {
        return json_encode(
            $arrayFilePaths,
            JSON_UNESCAPED_UNICODE |
            JSON_UNESCAPED_SLASHES |
            JSON_FORCE_OBJECT
        );
    }

    /**
     * @param $jsonFilePaths
     * @return mixed
     */
    protected static function jsonDecode($jsonFilePaths)
    {
        return json_decode($jsonFilePaths, true);
    }

    /**
     * @inheritDoc
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['project/view', 'id' => $this->id]),
        ];
    }
}
