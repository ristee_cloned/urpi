<?php


namespace common\models;


use Kreait\Firebase;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\MessageData;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class Push
 *
 * @package common\models
 *
 * @property string $body
 * @property string $title
 * @property array $data
 * @property string $type
 *
 * @property Firebase\Messaging $messaging
 * @property User $_user
 * @property string $credentialPath
 * @property Firebase\Factory $factory
 *
 */
class Push extends Model
{
    const COMMON_TOPIC = 'commonApp';
    const PUSH_TYPE_COMMON = 'common';
    const PUSH_TYPE_TOKEN = 'token';

    public $title = 'Сообщение от УРПИ';
    public $body = 'Сообщение от УРПИ';

    public $data = [];
    public $apn = [];
    public $type = self::PUSH_TYPE_COMMON;

    private $_user;
    private $messaging;
    private $credentialPath = '@common/config/credentials/google-service-account.json';
    private $factory;

    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['title', 'body'], 'trim'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'body' => 'Сообщение',
        ];
    }


    public function __construct(User $user = null)
    {
        $this->_user = $user;

        $googleServicePath = Url::to($this->credentialPath);

        $this->factory = (new Firebase\Factory())->withServiceAccount($googleServicePath);

        $this->messaging = $this->factory->createMessaging();
        parent::__construct();
    }

    public function send()
    {
        switch ($this->type) {
            case self::PUSH_TYPE_COMMON:
                $message = $this->getCommonMessage();
                break;
            case self::PUSH_TYPE_TOKEN:
                $message = $this->getPrivateMessage();
                break;
            default:
                throw new InvalidValueException('"type" не определен');
        }

        if (!$message) {
            return false;
        }

        try {
            $this->messaging->send($message);

            return true;
        } catch (MessagingException $e) {
            $this->addError('body', $e->getMessage());
        } catch (FirebaseException $e) {
            $this->addError('body', $e->getMessage());
        }

        return false;
    }

    /**
     * @return CloudMessage
     */
    private function getCommonMessage()
    {
        $message = CloudMessage::withTarget('topic', self::COMMON_TOPIC)
                          ->withData($this->getData());

        return $message;
    }

    /**
     * @return bool|CloudMessage
     */
    private function getPrivateMessage()
    {
        $user = $this->getUser();
        if (!$user || $this->hasErrors('_user')) {
            return false;
        }

        $pushToken = $user->push_token;
        if (empty($pushToken)) {
            $this->addError('_user', 'push_token не указан. Отправка не удалась.');

            return false;
        }

        $message = CloudMessage::withTarget('token', $pushToken)
                          ->withData($this->getData())
            ->withApnsConfig($this->getApnsConfig());

        return $message;
    }

    protected function getUser()
    {
        if (empty($this->_user)) {
            $this->addError('_user', 'Пользователь не определен');
            return null;
        }

        return $this->_user;
    }

    /**
     * @param $data
     * @return MessageData
     */
    protected function getData()
    {
        $this->checkData();

        if (!is_array($this->data)) {
            throw new InvalidValueException('Data должен быть массивом');
        }

        $this->data['title'] = $this->title;
        $this->data['body'] = $this->body;

        return MessageData::fromArray($this->data);
    }

    protected function getApnsConfig()
    {
        $this->checkData();
        $config = Firebase\Messaging\ApnsConfig::fromArray([
            'payload' => [
                'aps' => [
                    'alert' => [
                        'title' => $this->title,
                        'body' => $this->body,
                    ]
                ]
            ]
        ]);

        return $config;
    }

    private function checkData()
    {
        if (empty($this->body)) {
            throw new InvalidValueException('Body не может быть пустым');
        }

        if (empty($this->title)) {
            throw new InvalidValueException('Title не может быть пустым');
        }
    }
}
