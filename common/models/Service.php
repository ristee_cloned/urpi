<?php

namespace common\models;

use common\components\ResultTotal;
use common\helpers\StorageHelper;
use common\helpers\StorageHelperTrait;
use common\helpers\StorageHelperInterface;
use yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use moonland\phpexcel\Excel;
use yii\helpers\Url;
use yii\web\Linkable;

/**
* @SWG\Definition(definition="Service", type="object",
 *     required={"name", "measure", "price_min", "price_max", "service_category_id"},
 *      @SWG\Property(property="id", type="integer", example="32"),
 *      @SWG\Property(property="name", type="string", example="Услуга окрашивание"),
 *      @SWG\Property(property="description", type="string", example="Описание услуги"),
 *      @SWG\Property(property="content", type="string", example="Содержание услуги"),
 *      @SWG\Property(property="image", type="string", example="//example.com/upload/images/service/1583180010_filename.jpg"),
 *      @SWG\Property(property="measure", type="string", example="м2"),
 *      @SWG\Property(property="price_min", type="integer", example="10"),
 *      @SWG\Property(property="price_max", type="integer", example="20"),
 *      @SWG\Property(property="service_category_id", type="integer", example="10"),
 *      @SWG\Property(type="object", property="_links",
 *         @SWG\Property(type="object", property="self",
 *                @SWG\Property(type="string", property="href", example="/v1/service/32"),
 *         ),
 *         @SWG\Property(type="object", property="parent",
 *                @SWG\Property(type="string", property="href", example="/v1/service-category/10"),
 *         )
 *      )
 *  )
 */

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 * @property string $measure
 * @property int $price_min
 * @property int $price_max
 * @property int $service_category_id
 *
 * @property int $quantity
 * @property int $oldPrice
 *
 * @property ServiceCategory $serviceCategory
 */
class Service extends ActiveRecord implements StorageHelperInterface, Linkable
{
    public $quantity = 1; //TODO: добавить в описание услуги и использовать где нужно!
    public $oldPrice = null;

    public $user = null;

    use StorageHelperTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields[] = 'quantity';
        $fields['image'] = function() {
            return StorageHelper::getWebPath($this->image);
        };
        $fields['price'] = function() {
            $user = null;

            if (isset(Yii::$app->user) && Yii::$app->user) {
                /** @var User $user */
                $user = Yii::$app->user->identity;
            }

            return $this->getPrice($user);
        };

        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'measure',
                    'price_min',
                    'price_max',
                    'service_category_id',
                ],
                'required',
            ],
            [
                [
                    'name',
                    'measure',
                    'description',
                    'content',
                    'price_min',
                    'price_max',
                ],
                'trim',
            ],
            [['content'], 'string'],
            [['price_min', 'price_max', 'service_category_id'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['measure'], 'string', 'max' => 50],
            [['description', 'content', 'image'], 'string', 'max' => 255],
            [
                ['service_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ServiceCategory::class,
                'targetAttribute' => [
                    'service_category_id' => 'id',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'content' => 'Текст',
            'image' => 'Изображение',
            'measure' => 'Ед.изм.',
            'price_min' => 'Цена (мин.)',
            'price_max' => 'Цена (макс.)',
            'service_category_id' => 'Категория',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getServiceCategory()
    {
        return $this->hasOne(
            ServiceCategory::class,
            ['id' => 'service_category_id']
        );
    }

    /**
     * Imports services to a database from a file
     *
     * @param $file
     * @return bool
     */
    public static function import($file)
    {
        $data = Excel::import($file, ['setFirstRecordAsKeys' => true]);
        if (!$data) {
            return false;
        }
        $categories = ServiceCategory::getAllNamesWithIds();
        $services = self::getAll();
        /** @var array $data */
        foreach ($data as $item) {
            array_walk($item, function (&$value) {
                $value = trim($value);
            });
            if (isset($item['Category'],
                $item['ID'],
                $item['Name'],
                $item['Measure'],
                $item['Price-min'],
                $item['Price-max']
            )) {
                $catId = array_search($item['Category'], $categories);
                if (!$catId) {
                    $category = new ServiceCategory();
                    $category->name = $item['Category'];
                    $category->save();
                    $catId = $category->id;
                    $categories[$catId] = $category->name;
                }
                if (empty($item['ID'])) {
                    // create and save new service
                    $service = new self();
                    $service->service_category_id = $catId;
                    $service->name = $item['Name'];
                    $service->measure = $item['Measure'];
                    $service->price_min = $item['Price-min'];
                    $service->price_max = $item['Price-max'];
                    $service->save();
                } else {
                    // update all existing services
                    /** @var self $service */
                    foreach ($services as $service) {
                        if ($service->id != $item['ID']) {
                            continue;
                        }
                        if ($service->name != $item['Name']) {
                            $service->name = $item['Name'];
                        }
                        if ($service->measure != $item['Measure']) {
                            $service->measure = $item['Measure'];
                        }
                        if ($service->price_min != $item['Price-min']) {
                            $service->price_min = $item['Price-min'];
                        }
                        if ($service->price_max != $item['Price-max']) {
                            $service->price_max = $item['Price-max'];
                        }
                        if ($service->service_category_id != $catId) {
                            $service->service_category_id = $catId;
                        }
                        $service->save();
                    }
                }

            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array|ActiveRecord[]
     */
    public static function getAll()
    {
        return self::find()->all();
    }

    /**
     * Deletes the image file
     * @param null|string $path
     */
    public function deleteImageFile($path = null)
    {
        if (is_file($filename = Yii::getAlias('@storage' . $path))) {
            unlink($filename);
        }
    }

    /**
     * @inheritDoc
     */
    public function getLinks()
    {
        return [
            yii\web\Link::REL_SELF => Url::to(['service/view', 'id' => $this->id]),
            'parent' => Url::to(['service-category/view', 'id' => $this->service_category_id]),
        ];
    }

    /**
     * @param User|null $user
     * @return false|float|int
     */
    public function getPrice(?User $user = null)
    {
        if (empty($user)) {
            return $this->price_min;
        }

        if ($user->hasDiscount()) {
            return $this->getDiscount($this->price_min);
        }

        return $this->price_min;
    }

    public function getDiscount($amount)
    {
        $discount = Yii::$app->params['user.discount'];

        return ResultTotal::calculateDiscount($amount, $discount);
    }
}
