<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 *
 * @property string $email
 * @property string $password
 * @property bool $rememberMe
 *
 * @property User $_user
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ((!$user || !$user->validatePassword($this->password)) && !$this->hasErrors('email')) {
                $this->addError($attribute, 'Некорректный email или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findAnyByEmail($this->email);

            if ($this->_user) {
                if ($this->_user->status == User::STATUS_INACTIVE) {
                    $this->_user = null;
                    $this->addError('email', 'Аккаунт пользователя не активирован. Необходимо подтвердить аккаунт.');

                    return null;
                }

                if ($this->_user->status == User::STATUS_DELETED) {
                    $this->_user = null;
                    $this->addError('email', 'Пользователь удален.');

                    return null;
                }
            }
        }

        return $this->_user;
    }
}
