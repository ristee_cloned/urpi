<?php

namespace common\models;

use common\components\ResultTotal;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class Cart
 * @package common\models
 * @property array $services_data Data from frontend like [['id'=>1, 'count'=>10, 'price'=>30],...]
 * @property int $discount
 * @property string $images
 * @property string $comment
 *
 * @property Service[] $services
 * @property User $user
 */
class Cart extends Model
{
    const SCENARIO_CORPORATE = 'corporate';
    const SCENARIO_REGULAR = 'regular';

    const SERVICE_NOT_FOUND_CODE = "-1";

    public $user;
    public $files;
    public $comment;

    public $images;

    public $services;
    private $discount;
    private $services_data;

    /**
     * Cart constructor.
     * @param array $services data from user in format: [['id'=1, 'price'=>123, 'count'=>16]]
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->discount = Yii::$app->params['user.discount'];

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['services'], 'required', 'on' => self::SCENARIO_REGULAR],
            [['comment'], 'required', 'on' => self::SCENARIO_CORPORATE],
            ['comment', 'string', 'max' => 1000],
            ['comment', 'filter', 'filter' => function() {return Html::encode($this->comment);}],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_REGULAR] = ['services'];
        $scenarios[self::SCENARIO_CORPORATE] = ['comment'];

        return $scenarios;
    }


    /**
     * @return Service[]
     */
    public function getServices()
    {
        return $this->services;
    }

    public function getServicesTotal()
    {
        $total = 0;

        foreach ($this->services as $service) {
            $total += $service->price_min * $service->quantity;
        }

        if ($this->user && $this->user->hasDiscount()) {
            ResultTotal::calculateDiscount($total, $this->discount);
        }

        return $total;
    }

    public function checkServices()
    {
        $compare = [];
        foreach ($this->services_data as $cartService) {
            $cartServiceId = $cartService['id'];
            if (isset($this->services[$cartServiceId])) {
                $service = $this->services[$cartServiceId];
                $servicePrice = $service->price_min;
                if ($this->user && $this->user->hasDiscount()) {
                    $servicePrice = ResultTotal::calculateDiscount($servicePrice, $this->discount);
                }

                if ($servicePrice != $service->oldPrice) {
                    $compare[$cartServiceId] =$servicePrice;
                }
            } else {
                $compare[$cartServiceId] = self::SERVICE_NOT_FOUND_CODE;
            }
        }

        return $compare;
    }

    private function getServicesFromData()
    {
        $servicesId = [];
        $oldPrice = [];
        foreach ($this->services_data as $data) {
            $serviceId = $data['id'];
            $servicesId[] = $serviceId;
            $quantity[$serviceId] = $data['count'];
            $oldPrice[$serviceId] = $data['price'];
        }

        $this->services = Service::find()
                                 ->indexBy('id')
                                 ->where(['id' => $servicesId])
                                 ->all();

        foreach ($this->services as $service) {
            $service->quantity = 0;
            $service->oldPrice = null;

            if (isset($quantity[$service->id])) {
                $service->quantity = (int)$quantity[$service->id];
            }
            if (isset($oldPrice[$service->id])) {
                $service->oldPrice = (int)$oldPrice[$service->id];
            }
        }
    }

    public function load($data, $formName = null)
    {
        $parent = parent::load($data, $formName);

        if (!empty($this->services)) {
            $this->services_data = Json::decode($this->services);
            $this->getServicesFromData();
        }

        return $parent;
    }
}
