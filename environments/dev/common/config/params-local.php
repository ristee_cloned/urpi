<?php
return [
    // to display images correctly on the backend
    'frontendBaseUrl' => '//urpi.loc',
    'storageBaseUrl' => '//urpi.loc',
    'apiHost' => 'api.urpi.loc',
    'robokassa.login' => '',
    'robokassa.password' => '',
    'robokassa.password2' => '',
    'robokassa.isTest' => true,
];
