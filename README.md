Консольные команды
------------------

#### Frontend (folder: frontend/vue)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Смена ссылок на API и версию API (файл: frontend/vue/.env )

```
API_URL - ссылка на API
API_VERSION - версия API
```

#### Создание пользователя

Для создания администратора нужно ввести следующую команду:

```
./yii user/create-admin email password
```

где email - адрес электронной почты пользователя, а password - пароль

#### Изменение пароля пользователя

Для изменения пароля пользователя нужно ввести команду:

```
./yii user/update-password email password
```

где email - адрес пользователя, которому нужно изменить пароль, а password - новый пароль пользователя

## Методы API

#### Аутентификация

POST /api/v1/user/login , где v1 - модуль (версия API)

Передать через Body логин и пароль пользователя в json формате.
При каждой аутентификации возвращается новый токен, со сроком действия на одни сутки.

Response (Status 200): OK

```
{
    "token": "0H-c7_HI50fz3pwyvsFpDCkicqmfhAiX",
    "expired": "2020-02-15T00:21:25+05:00"
}
```

Response (Status 422): некорректный пароль или email

```
{
    "field": "password",
    "message": "Некорректный email или пароль."
}
```

Response (Status 422): пустой пароль или email

```
{
    "field": "email",
    "message": "Необходимо заполнить «Email»."
}
```

Response (Status 405): запрещенный метод HTTP

```
{
    "name": "Method Not Allowed",
    "message": "Method Not Allowed. This URL can only handle the following request methods: POST.",
    "code": 0,
    "status": 405,
    "type": "yii\\web\\MethodNotAllowedHttpException"
}
```

#### Регистрация

POST /api/v1/user/signup

Передать через Body все поля, необходимые для регитрации пользователя, в json формате.

Response (Status 200): OK - вернет true

Response (Status 422): пустое поле, обязатльное для заполнения

```
{
    "field": "email",
    "message": "Необходимо заполнить «Email»."
}
```

Response (Status 422): некорректное значение поля

```
{
    "field": "email",
    "message": "Значение «Email» не является правильным email адресом."
}
```

Response (Status 422): проверка на уникальность

```
{
    "field": "email",
    "message": "Этот адрес электронной почты уже занят."
}
```

Response (Status 405): запрещенный метод HTTP

```
{
    "name": "Method Not Allowed",
    "message": "Method Not Allowed. This URL can only handle the following request methods: GET.",
    "code": 0,
    "status": 405,
}
```

#### Подтверждение email-а пользователя при регистрации

GET /api/v1/user/verify-email?token=IH9Rmb2rS\_-_Q1Z75VPgn1kUGcOH-M2b_1581703990

Пользователю приходит письмо со ссылкой для подтверждения, в которой присутствует токен верификации.

Response (Status 200): OK - вернет true

Response (Status 400): OK неправильно подтвержден токен

```
{
    "name": "Bad Request",
    "message": "Неправильно подтвержден токен электронной почты.",
    "code": 0,
    "status": 400,
    "type": "yii\\web\\BadRequestHttpException"
}
```

Response (Status 422): пустой токен или отсутствует

```
{
    "field": "token",
    "message": "Необходимо заполнить «Token»."
}
```

Response (Status 405): запрещенный метод HTTP

```
{
    "name": "Method Not Allowed",
    "message": "Method Not Allowed. This URL can only handle the following request methods: GET.",
    "code": 0,
    "status": 405,
}
```
