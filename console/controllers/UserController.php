<?php

namespace console\controllers;

use common\models\User;
use yii\helpers\Console;
use yii\console\Controller;

/**
 * Class UserController
 * @package console\controllers
 */
class UserController extends Controller
{
    /**
     * @param $email
     * @param $password
     */
    public function actionCreateAdmin($email, $password)
    {
        $user = new User();
        $user->email = $email;
        $user->status = User::STATUS_ACTIVE;
        $user->type = User::TYPE_ADMIN;
        $user->setPassword($password);
        $user->generateAuthKey();

        if ($user->save()) {
            $this->printSuccessCreate($email, $password);
        }
    }

    /**
     * @param $email
     * @param $password
     */
    public function actionUpdatePassword($email, $password)
    {
        $user = User::findByEmail($email);

        if ($user) {
            $user->setPassword($password);

            if ($user->save()) {
                $this->printSuccessUpdate($password);
            }
        } else {
            $this->printNotFoundUser($email);
        }
    }

    /**
     * @param $email
     * @param $password
     */
    protected function printSuccessCreate($email, $password)
    {
        echo 'Created user: ' . $email . ' => ' . $password . PHP_EOL;
        $this->printDone();
    }

    /**
     * @param $password
     */
    protected function printSuccessUpdate($password)
    {
        echo 'Updated user password: ' . $password . PHP_EOL;
        $this->printDone();
    }

    /**
     * @param $email
     */
    protected function printNotFoundUser($email)
    {
        echo $this->ansiFormat(
            'User not found or inactive: ' . $email . PHP_EOL,
            Console::FG_RED
        );
        $this->printDone();
    }

    /**
     * Prints a command completion message
     */
    protected function printDone()
    {
        echo $this->ansiFormat('Done!', Console::FG_CYAN) . PHP_EOL;
    }
}
