<?php

use yii\db\Migration;

/**
 * Class m200209_194937_alter_paid_at_column_in_payment_table
 */
class m200209_194937_alter_paid_at_column_in_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn(
            '{{%payment}}',
            'paid_at',
            $this->integer()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->alterColumn(
            '{{%payment}}',
            'paid_at',
            $this->integer()->notNull()
        );
    }
}
