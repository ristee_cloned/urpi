<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_profile}}`.
 */
class m200119_191812_create_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%user_profile}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'surname' => $this->string(50)->notNull(),
            'phone' => $this->string(11)->notNull(),
            'address' => $this->string(),
            'company_name' => $this->string(),
            'inn' => $this->string(12),
            'discount_card' => $this->string(),
            'avatar' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-user_profile-user',
            '{{%user_profile}}',
            'id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%user_profile}}');
    }
}
