<?php

use yii\db\Migration;

/**
 * Class m200311_181500_request_add_total_field
 */
class m200311_181500_request_add_total_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%request}}',
            'total',
            $this->integer()
                 ->unsigned()
                 ->defaultValue(0)
                 ->notNull()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%request}}', 'total');
    }
}
