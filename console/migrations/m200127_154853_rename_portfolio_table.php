<?php

use yii\db\Migration;

/**
 * Class m200127_154853_rename_portfolio_table
 */
class m200127_154853_rename_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->renameTable('{{%portfolio}}', '{{%project}}');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->renameTable('{{%project}}', '{{%portfolio}}');
    }
}
