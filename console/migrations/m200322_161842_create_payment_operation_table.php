<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_operation}}`.
 */
class m200322_161842_create_payment_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_operation}}', [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer(),
            'out_sum' => $this->float(2),
            'signature' => $this->string(),
            'description' => $this->string(500),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'url' => $this->string(500),
        ]);
        $this->addForeignKey('{{%fk__payment_operation__payment__payment_id__id}}', '{{%payment_operation}}', 'payment_id', '{{%payment}}', 'id', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_operation}}');
    }
}
