<?php

use yii\db\Migration;

/**
 * Class m200318_045019_request_add_images_field
 */
class m200318_045019_request_add_images_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%request}}', 'images', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%request}}', 'images');
    }
}
