<?php

use yii\db\Migration;

/**
 * Class m200122_114916_alter_service_table
 */
class m200122_114916_alter_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn(
            '{{%service}}',
            'description',
            $this->string()
        );
        $this->alterColumn(
            '{{%service}}',
            'content',
            $this->string()
        );
        $this->alterColumn(
            '{{%service}}',
            'image',
            $this->string()
        );
        $this->renameColumn(
            '{{%service}}',
            'price',
            'price_min'
        );
        $this->addColumn(
            '{{%service}}',
            'price_max',
            $this->integer()
                ->notNull()
                ->after('price_min')
        );
        $this->addColumn(
            '{{%service}}',
            'measure',
            $this->string(50)
                ->notNull()
                ->after('image')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn(
            '{{%service}}',
            'measure'
        );
        $this->dropColumn(
            '{{%service}}',
            'price_max'
        );
        $this->renameColumn(
            '{{%service}}',
            'price_min',
            'price'
        );
        $this->alterColumn(
            '{{%service}}',
            'image',
            $this->string()->notNull()
        );
        $this->alterColumn(
            '{{%service}}',
            'content',
            $this->string()->notNull()
        );
        $this->alterColumn(
            '{{%service}}',
            'description',
            $this->string()->notNull()
        );
    }
}
