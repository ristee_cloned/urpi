<?php

use yii\db\Migration;

/**
 * Class m200131_175503_alter_request_table
 */
class m200131_175503_alter_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropColumn('{{%request}}', 'confirmed');
        $this->dropColumn('{{%request}}', 'in_work');
        $this->dropColumn('{{%request}}', 'closed');
        $this->dropColumn('{{%request}}', 'rejected');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->addColumn('{{%request}}', 'rejected', $this->integer());
        $this->addColumn('{{%request}}', 'closed', $this->integer());
        $this->addColumn('{{%request}}', 'in_work', $this->integer());
        $this->addColumn('{{%request}}', 'confirmed', $this->integer());
    }
}
