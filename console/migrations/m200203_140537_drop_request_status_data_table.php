<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%request_status_data}}`.
 */
class m200203_140537_drop_request_status_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%request_status_data}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%request_status_data}}', [
            'request_id' => $this->integer(),
            'request_status_id' => $this->integer(),
            'PRIMARY KEY(request_id, request_status_id)',
            'status_time' => $this->integer()->notNull(),
        ]);

        // creates index for column `request_id`
        $this->createIndex(
            '{{%idx-request_status_data-request_id}}',
            '{{%request_status_data}}',
            'request_id'
        );

        // add foreign key for table `{{%request}}`
        $this->addForeignKey(
            '{{%fk-request_status_data-request_id}}',
            '{{%request_status_data}}',
            'request_id',
            '{{%request}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        // creates index for column `request_status_id`
        $this->createIndex(
            '{{%idx-request_status_data-request_status_id}}',
            '{{%request_status_data}}',
            'request_status_id'
        );

        // add foreign key for table `{{%request_status}}`
        $this->addForeignKey(
            '{{%fk-request_status_data-request_status_id}}',
            '{{%request_status_data}}',
            'request_status_id',
            '{{%request_status}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }
}
