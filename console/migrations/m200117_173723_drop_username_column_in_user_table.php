<?php

use yii\db\Migration;

/**
 * Handles the dropping of username column in `{{%user}}`.
 */
class m200117_173723_drop_username_column_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropColumn('{{%user}}', 'username');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->addColumn('{{%user}}', 'username', $this->string()->notNull()->unique());
    }
}
