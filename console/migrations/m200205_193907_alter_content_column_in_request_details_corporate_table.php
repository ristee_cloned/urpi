<?php

use yii\db\Migration;

/**
 * Class m200205_193907_alter_content_column_in_request_details_corporate_table
 */
class m200205_193907_alter_content_column_in_request_details_corporate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn(
            '{{%request_details_corporate}}',
            'content',
            $this->text()->notNull()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->alterColumn(
            '{{%request_details_corporate}}',
            'content',
            $this->string()->notNull()
        );
    }
}
