<?php

use yii\db\Migration;

/**
 * Class m200205_190633_alter_request_details_corporate_table
 */
class m200205_190633_alter_request_details_corporate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropForeignKey(
            'fk-request_details_corporate-request_id',
            '{{%request_details_corporate}}'
        );
        $this->dropIndex(
            'idx-request_details_corporate-request_id',
            '{{%request_details_corporate}}'
        );
        $this->dropColumn(
            '{{%request_details_corporate}}',
            'request_id'
        );
        $this->addForeignKey(
            'fk-request_details_corporate-request',
            '{{%request_details_corporate}}',
            'id',
            '{{%request}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-request_details_corporate-request',
            '{{%request_details_corporate}}'
        );
        $this->addColumn(
            '{{%request_details_corporate}}',
            'request_id',
            $this->integer()->notNull()
        );
        $this->createIndex(
            'idx-request_details_corporate-request_id',
            '{{%request_details_corporate}}',
            'request_id'
        );
        $this->addForeignKey(
            'fk-request_details_corporate-request_id',
            '{{%request_details_corporate}}',
            'request_id',
            '{{%request}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }
}
