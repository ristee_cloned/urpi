<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%request_details_regular}}`.
 */
class m200209_175632_add_image_column_to_request_details_regular_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn(
            '{{%request_details_regular}}',
            'image',
            $this->string()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn(
            '{{%request_details_regular}}',
            'image'
        );
    }
}
