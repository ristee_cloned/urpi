<?php

use yii\db\Migration;

/**
 * Class m200330_044908_user_add_push_token
 */
class m200330_044908_user_add_push_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'push_token', $this->string(1000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'push_token');
    }
}
