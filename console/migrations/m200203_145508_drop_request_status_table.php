<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%request_status}}`.
 */
class m200203_145508_drop_request_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropTable('{{%request_status}}');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->createTable('{{%request_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->batchInsert(
            '{{%request_status}}',
            ['name'],
            [
                ['Новая'],
                ['Подтверждена'],
                ['В работе'],
                ['Оплачена'],
                ['Закрыта'],
                ['Отклонена'],
            ]
        );
    }
}
