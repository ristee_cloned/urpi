<?php

use yii\db\Migration;

/**
 * Class m200203_160822_alter_request_table
 */
class m200203_160822_alter_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn(
            '{{%request}}',
            'appointed_at',
            $this->integer()->after('updated_at')
        );
        $this->addColumn(
            '{{%request}}',
            'executor',
            $this->string()->after('content')
        );
        $this->addColumn(
            '{{%request}}',
            'status_updated_at',
            $this->integer()->after('status')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn(
            '{{%request}}',
            'status_updated_at'
        );
        $this->dropColumn(
            '{{%request}}',
            'executor'
        );
        $this->dropColumn(
            '{{%request}}',
            'appointed_at'
        );
    }
}
