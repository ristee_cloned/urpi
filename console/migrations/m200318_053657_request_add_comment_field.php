<?php

use yii\db\Migration;

/**
 * Class m200318_053657_request_add_comment_field
 */
class m200318_053657_request_add_comment_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%request}}', 'comment', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%request}}', 'comment');
    }
}
