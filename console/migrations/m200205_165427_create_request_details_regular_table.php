<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_details_regular}}`.
 */
class m200205_165427_create_request_details_regular_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%request_details_regular}}', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'service_name' => $this->string()->notNull(),
            'quantity' => $this->integer()->notNull()->unsigned(),
            'price' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-request_details_regular-request_id',
            '{{%request_details_regular}}',
            'request_id'
        );
        $this->addForeignKey(
            'fk-request_details_regular-request_id',
            '{{%request_details_regular}}',
            'request_id',
            '{{%request}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%request_details_regular}}');
    }
}
