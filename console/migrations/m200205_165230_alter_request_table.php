<?php

use yii\db\Migration;

/**
 * Class m200205_165230_alter_request_table
 */
class m200205_165230_alter_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropColumn(
            '{{%request}}',
            'content'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->addColumn(
            '{{%request}}',
            'content',
            $this->text()->notNull()
        );
    }
}
