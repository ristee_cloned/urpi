<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_details_corporate}}`.
 */
class m200205_165504_create_request_details_corporate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%request_details_corporate}}', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'content' => $this->string()->notNull(),
        ]);

        $this->createIndex(
            'idx-request_details_corporate-request_id',
            '{{%request_details_corporate}}',
            'request_id'
        );
        $this->addForeignKey(
            'fk-request_details_corporate-request_id',
            '{{%request_details_corporate}}',
            'request_id',
            '{{%request}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%request_details_corporate}}');
    }
}
