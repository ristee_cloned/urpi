<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service}}`.
 */
class m200121_144957_create_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'description' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'image' => $this->string()->notNull(),
            'price' => $this->integer()->notNull(),
            'service_category_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-service-service_category_id',
            '{{%service}}',
            'service_category_id'
        );
        $this->addForeignKey(
            'fk-service-service_category',
            '{{%service}}',
            'service_category_id',
            '{{%service_category}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%service}}');
    }
}
