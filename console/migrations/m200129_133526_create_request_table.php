<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request}}`.
 */
class m200129_133526_create_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%request}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'confirmed' => $this->integer(),
            'in_work' => $this->integer(),
            'closed' => $this->integer(),
            'rejected' => $this->integer(),
            'type' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'content' => $this->text()->notNull(),
            'vote' => $this->smallInteger(),
        ]);
        $this->createIndex(
            'idx-request-user_id',
            '{{%request}}',
            'user_id'
        );
        $this->addForeignKey(
            'fk-request-user',
            '{{%request}}',
            'user_id',
            '{{%user}}',
            'id',
            'SET NULL',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%request}}');
    }
}
