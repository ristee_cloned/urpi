<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request_status}}`.
 */
class m200131_181349_create_request_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%request_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->batchInsert(
            '{{%request_status}}',
            ['name'],
            [
                ['Новая'],
                ['Подтверждена'],
                ['В работе'],
                ['Оплачена'],
                ['Закрыта'],
                ['Отклонена'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%request_status}}');
    }
}
