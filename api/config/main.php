<?php

use yii\web\Response;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'format' =>  Response::FORMAT_JSON,
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'api\common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/user', 'only' => ['index', 'set-push', 'delete-push'],'pluralize' => false,
                    'patterns' => [
                        'POST /push' => 'set-push',
                        'DELETE push' => 'delete-push',
                    ],
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/service-category', 'pluralize' => false,],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/service', 'pluralize' => false,],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/post', 'pluralize' => false,],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/project', 'pluralize' => false,],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/payment', 'pluralize' => false,
                    'extraPatterns' => [
                        'POST pay/<id:\d+>' => 'pay',
                    ]
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/request', 'pluralize' => false,
                    'extraPatterns' => [
                        'POST /register' => 'register',
                        'POST vote/<id:\d+>' => 'vote',
                    ],
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/user-profile', 'pluralize' => false,
                    'patterns' => [
                        'GET /' => 'index',
                        'POST /' => 'update',
                    ]
                ],
                'docs' => 'site/docs',
            ],
        ],
    ],
    'params' => $params,
];
