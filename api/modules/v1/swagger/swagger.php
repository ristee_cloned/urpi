<?php

namespace api\modules\v1\swagger;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     basePath="/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="УРПИ АПИ",
 *         description="Version: __2.0.0__<br>
 *              Для авторизации через Bearer в **swagger** необходимо перед токеном добавить Bearer. <br>
 *              Пример: `Bearer lTF6xDdfEp3CsVxLHigeqx8oY9uZlnL`",
 *         @SWG\Contact(name = "RuYOU", email = "support@ruyou.ru")
 *     ),
 * )
 *
 */

/**
 * @SWG\Definition(
 *   @SWG\Xml(name="##default")
 * )
 */
class ApiResponse
{
    /**
     * @SWG\Property(format="int32", description = "code of result")
     * @var int
     */
    public $code;
    /**
     * @SWG\Property
     * @var string
     */
    public $type;
    /**
     * @SWG\Property
     * @var string
     */
    public $message;
    /**
     * @SWG\Property(format = "int64", enum = {1, 2})
     * @var integer
     */
    public $status;
}
