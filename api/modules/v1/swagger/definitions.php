<?php

namespace app\models\v1\swagger;

/**
 * @SWG\Definition(
 *      definition="Error",
 *      required={"code", "message"},
 *      @SWG\Property(
 *          property="name",
 *          type="integer",
 *          format="string",
 *          example="Unauthorized",
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          type="string",
 *          example="You are requesting with an invalid credential."
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          type="integer",
 *          format="int32",
 *          example=0
 *      ),
 *     @SWG\Property(
 *          property="status",
 *          type="integer",
 *          format="int32",
 *          example=401
 *      ),
 * ),
 *
 * @SWG\Definition(
 *     definition="ValidationError",
 *     required={"field", "message"},
 *         @SWG\Property(
 *           property="field",
 *           type="string",
 *           format="string",
 *           example="services",
 *         ),
 *         @SWG\Property(
 *           property="message",
 *           type="string",
 *           format="string",
 *           example="Необходимо заполнить «Services».",
 *         ),
 *     ),
 *  )
 */