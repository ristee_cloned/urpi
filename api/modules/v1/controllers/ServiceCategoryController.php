<?php


namespace api\modules\v1\controllers;


use api\common\controllers\ApiController;
use common\models\Service;
use common\models\ServiceCategory;
use Swagger\Annotations as SWG;
use yii\rest\ActiveController;

class ServiceCategoryController extends ApiController
{
    public $modelClass = ServiceCategory::class;

    /**
     * @SWG\Get(path="/service-category",
     *     tags={"Категории услуг"},
     *     summary="Категории услуг",
     *     description="Список категорий услуг",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *         response=200,
     *     description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 ref="#/definitions/ServiceCategory",
     *            )
     *         )
     *     )
     * )
     */

    /**
     * @SWG\Get(path="/service-category/{id}",
     *     tags={"Категории услуг"},
     *     summary="Услуги",
     *     description="Список услуг",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *        in = "path",
     *        name = "id",
     *        description = "ID категории",
     *        required = true,
     *        type = "integer"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = " success",
     *          @SWG\Schema(ref="#/definitions/ExtendedServiceCategory")
     *     )
     * )
     *
     */
}
