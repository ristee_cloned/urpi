<?php

namespace api\modules\v1\controllers;

use api\common\controllers\ApiController;
use api\modules\v1\models\User;
use common\models\Cart;
use common\models\form\ImagesUploadForm;
use common\models\Request;
use Swagger\Annotations as SWG;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class RequestController
 * @package api\modules\v1\controllers
 */
class RequestController extends ApiController
{
    public $modelClass = 'api\modules\v1\models\Request';

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['register'] = ['POST'];
        $verbs['statuses'] = ['GET'];
        $verbs['vote'] = ['POST'];

        return $verbs;
    }


    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = function ($action) {
            $userId = Yii::$app->user->getId();

            return new ActiveDataProvider([
                'query' => Request::find()->where(['user_id' => $userId]),
            ]);
        };
        $actions[] = 'statuses';

        return $actions;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        $actions = ['view', 'vote'];
        if (in_array($action, $actions)) {
            $currentUserId = Yii::$app->user->getId();
            if ($model->user_id != $currentUserId) {
                throw new ForbiddenHttpException('Доступ запрещен.');
            }
        }

        parent::checkAccess($action, $model, $params);
    }

    /**
     * @SWG\Get(path="/request",
     *     tags={"Заявка"},
     *     summary="Список заказов",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *            type="object",
     *            ref="#/definitions/Request",
     *          )
     *        )
     *     )
     * )
     */

    /**
     * @SWG\Get(path="/request/{id}",
     *     tags={"Заявка"},
     *     summary="Информация по заявке",
     *     description="",
     *     @SWG\Parameter(
     *         in="path",
     *         name="id",
     *         description="ID заявки",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/Request",
     *        )
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Доступ запрещен",
     *     )
     * )
     */

    /**
     * @SWG\Post(path="/request/register",
     *     tags={"Заявка"},
     *     summary="Регистрация заявки (оформление корзины)",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *        in="formData",
     *        name="services",
     *        description="Услуги в JSON в формате: [{'id':2, 'count':10,'price':30}]. Обязательно для обычного пользователя",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="images[]",
     *        description="Файлы изображений",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="comment",
     *        description="Комментарий к заказу. Обязательно для корпоративного пользователя",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/Request",
     *        )
     *     ),
     *     @SWG\Response(
     *        response="422",
     *        description="Data Validation Failed",
     *        @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              ref="#/definitions/ValidationError",
     *          )
     *       )
     *     )
     * )
     * @return array|bool|Request|string
     * @throws \yii\base\Exception
     */
    public function actionRegister()
    {
        $user = $this->getUser();
        $postData = Yii::$app->request->post();

        $cart = new Cart();
        $cart->user = $user;

        switch ($user->type) {
            case User::TYPE_REGULAR:
                $cart->scenario = Cart::SCENARIO_REGULAR;
                $cart->load($postData, '');
                if ($cart->validate()) {

                    if (!$cart->validate()) {
                        return $cart;
                    }

                    $serviceCheckResult = $cart->checkServices();
                    if (!empty($serviceCheckResult)) {
                        return [
                            'success' => false,
                            'data' => $serviceCheckResult,
                        ];
                    }

                    if (!$this->uploadImages($cart)) {
                        return $cart;
                    }

                    $request = new Request($cart);

                    if ($request->save()) {
                        return $request;
                    }

                    return $request;
                } else {
                    return $cart;
                }

                break;
            case User::TYPE_CORPORATE:
                $cart->scenario = Cart::SCENARIO_CORPORATE;
                $cart->load($postData, '');

                if (!$cart->validate()) {
                    return $cart;
                }

                if (!$this->uploadImages($cart)) {
                    return $cart;
                }

                $request = new Request($cart);

                if ($request->save()) {
                    return $request;
                }

                return $request;
        }

        throw new BadRequestHttpException('Неверный запрос');
    }

    /**
     * @SWG\Get(path="/request/statuses",
     *     tags={"Заявка"},
     *     summary="Статусы заявок",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *          @SWG\Items(
     *              type="object",
     *              example={"1":"Новая", "2":"Подтверждена"}
     *          )
     *     )
     * )
     *
     * @return array
     */
    public function actionStatuses()
    {
        return Request::getStatuses();
    }

    /**
     * @SWG\Post(path="/request/vote/{id}",
     *     tags={"Заявка"},
     *     summary="Оценка выполнения заявки",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *       in="path",
     *       name="id",
     *       description="ID заявки",
     *       required=true,
     *       type="integer",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="vote",
     *        description="Голос из спика: 1 - позитивный, -1 - негативный. 0 - нейтральный, но передаваться не должен",
     *        required=true,
     *        type="integer",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          @SWG\Property(type="string", property="success", example="true"),
     *          @SWG\Property(type="string", property="vote", example="-1"),
     *        )
     *     )
     * )
     *
     * @param $id
     * @return array|Request|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws NotAcceptableHttpException
     */
    public function actionVote($id)
    {
        $model = $this->findModel($id);
        $this->checkAccess('vote', $model);

        if (!$model->canVote()) {
            throw new NotAcceptableHttpException('Голосование невозможно.');
        }

        $model->scenario = Request::SCENARIO_VOTE;
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate()) {
            if ($model->save()) {
                return [
                    'success' => true,
                    'vote' => $model->vote,
                ];
            }

            return $model;
        }

        return $model;
    }

    private function getUser()
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;

        return $currentUser;
    }

    /**
     * @param Cart $cart
     * @return bool
     * @throws \yii\base\Exception
     */
    private function uploadImages(Cart $cart)
    {
        $requestImagePath = Yii::$app->params['request.imageRelPath'];
        $requestImagesUploadCount = Yii::$app->params['request.imagesUploadCount'];

        $uploadForm = new ImagesUploadForm($requestImagePath);
        $uploadForm->maxFiles = $requestImagesUploadCount;
        $uploadForm->uploadFile = UploadedFile::getInstancesByName('images');

        if (!empty($uploadForm)) {
            if ($files = $uploadForm->upload()) {
                $cart->images = $files;
            }

            if ($uploadForm->hasErrors('uploadFile')) {
                $cart->addError('images', $uploadForm->getFirstError('uploadFile'));

                return false;
            }
        }

        return true;
    }

    /**
     * @param $id
     * @return Request|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) == null) {
            throw new NotFoundHttpException('Not Found.');
        }

        return $model;
    }
}
