<?php


namespace api\modules\v1\controllers;


use api\common\controllers\ApiController;
use common\models\About;
use Swagger\Annotations as SWG;
use yii\web\NotFoundHttpException;

class AboutController extends ApiController
{
    public $modelClass = About::class;

    public function actions()
    {
        return [
            'index',
        ];
    }

    /**
     * @SWG\Get(path="/about",
     *     tags={"О компании"},
     *     summary="О компании",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/About",
     *        )
     *     )
     * )
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = About::findOneModel();
        if (empty($model)) {
            throw new NotFoundHttpException('Нет данных');
        }

        return $model;
    }
}
