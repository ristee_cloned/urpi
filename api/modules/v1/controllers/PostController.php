<?php


namespace api\modules\v1\controllers;


use api\common\controllers\ApiController;
use common\models\Post;

class PostController extends ApiController
{
    public $modelClass = Post::class;

    /**
     * @SWG\Get(path="/post",
     *     tags={"Новости"},
     *     summary="Новости",
     *     description="Список всех новостей",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *         response = 200,
     *         description = "success",
     *         @SWG\Schema(
     *           type="array",
     *           @SWG\Items(
     *              type="object",
     *              ref="#/definitions/Post",
     *           )
     *        )
     *     )
     * )
     *
     */

    /**
     * @SWG\Get(path="/post/{id}",
     *     tags={"Новости"},
     *     summary="Новость",
     *     description="Выбранная новость",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *        in = "path",
     *        name = "id",
     *        description = "ID новости",
     *        required = true,
     *        type = "integer"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = " success",
     *          @SWG\Schema(ref="#/definitions/Post")
     *     )
     * )
     *
     */
}