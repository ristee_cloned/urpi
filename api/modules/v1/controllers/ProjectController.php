<?php


namespace api\modules\v1\controllers;


use api\common\controllers\ApiController;
use common\models\Project;

class ProjectController extends ApiController
{
    public $modelClass = Project::class;

    /**
     * @SWG\Get(path="/project",
     *     tags={"Портфолио"},
     *     summary="Портфолио",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *         response=200,
     *     description="success",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 ref="#/definitions/Project",
     *            )
     *         )
     *     )
     * )
     */

    /**
     * @SWG\Get(path="/project/{id}",
     *     tags={"Портфолио"},
     *     summary="Портфолио",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *        in = "path",
     *        name = "id",
     *        description = "ID категории",
     *        required = true,
     *        type = "integer"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = " success",
     *          @SWG\Schema(ref="#/definitions/Project",)
     *     )
     * )
     *
     */
}