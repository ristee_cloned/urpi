<?php

namespace api\modules\v1\controllers;

use api\common\controllers\ApiController;
use api\modules\v1\models\ChangePasswordForm;
use api\modules\v1\models\FeedbackForm;
use api\modules\v1\models\User;
use frontend\models\PasswordResetRequestForm;
use Swagger\Annotations as SWG;
use Yii;
use api\modules\v1\models\LoginForm;
use api\modules\v1\models\SignupForm;
use api\modules\v1\models\VerifyEmailForm;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Class UserController
 * @package api\modules\v1\controllers
 */
class UserController extends ApiController
{
    public $modelClass = 'api\modules\v1\models\User';

    /**
     * @return array
     */
    public function actions()
    {
        return [
//            'login',
//            'signup',
//            'verify-email',
//            'me',
        ];
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'login' => ['post'],
            'signup' => ['post'],
            'verify-email' => ['get'],
            'me' => ['get'],
            'change-password' => ['post'],
            'request-password-reset' => ['post'],
            'set-push' => ['post'],
            'delete-push' => ['delete'],
        ];
    }

    /**
     *
     * @SWG\Post(path="/user/login",
     *     tags={"Пользователь"},
     *     summary="Авторизация пользователя, получение токена",
     *     description="",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *        in = "formData",
     *        name = "email",
     *        description = "Email пользователя",
     *        required = true,
     *        type = "string"
     *     ),
     *      @SWG\Parameter(
     *        in = "formData",
     *        name = "password",
     *        description = "Пароль пользователя",
     *        required = true,
     *        type = "string"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = " success",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="token", type="string", example="1TF6xDdfEp3CsVxLHigeqx8oY9uZlnL"),
     *              @SWG\Property(property="type", type="integer", example="1"),
     *          )
     *     ),
     *     @SWG\Response(
     *         response = 401,
     *         description = "false",
     *     )
     * )
     *
     * @return LoginForm|\api\common\models\Token|null
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($data = $model->login()) {            
            return $data;//$token;
        } else {
            return $model;
        }
    }

    /**
     * @SWG\Post(path="/user/signup",
     *     tags={"Пользователь"},
     *     summary="Регистрация пользователя",
     *     description="",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *        in="formData",
     *        name="email",
     *        description="Емейл",
     *        required=true,
     *        type="string",
     *        maxLength=255,
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="password",
     *        description="Пароль",
     *        required=true,
     *        type="string",
     *        minLength=6,
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="type",
     *        description="Тип пользователя: 0 - обычный, 1 - корпоративный",
     *        required=true,
     *        type="integer",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="name",
     *        description="Имя",
     *        required=true,
     *        type="string",
     *        maxLength=50,
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="surname",
     *        description="Фамилия",
     *        required=true,
     *        type="string",
     *        maxLength=50,
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        description="Телефон",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="address",
     *        description="Адрес",
     *        required=true,
     *        type="string",
     *        maxLength=255,
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="inn",
     *        description="ИНН",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="company",
     *        description="Компания",
     *        required=false,
     *        type="string",
     *        maxLength=255,
     *     ),
     *
     *     @SWG\Response(
     *        response="200",
     *        description=" success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/SignupForm"
     *        )
     *     )
     * )
     *
     * @return SignupForm|bool
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->bodyParams, '');
        Yii::$app->mailer->view->params['url'] = Url::toRoute(
            'user/verify-email'
        );
        if ($model->signup()) {
            Yii::$app->mailer->view->params['url'] = null;
            unset($model['password']);
            return $model;
        } else {
            return $model;
        }
    }

    /**
     * @SWG\Get(path="/user/me",
     *     tags={"Пользователь"},
     *     summary="Описание текущего пользователя",
     *     description="",
     *     produces={"application/json", "application/xml"},
     *     security={{"Bearer":{}}},
     *
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *       @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="id", type="integer", example=1),
     *          @SWG\Property(property="email", type="string", example="ivanov@example.ru"),
     *          @SWG\Property(property="status", type="integer", example="10"),
     *          @SWG\Property(property="type", type="integer", example="0"),
     *       )
     *     )
     * )
     *
     *
     * @return User|null
     * @throws \Throwable
     *
     *
     */
    public function actionMe()
    {
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();
        unset($user['auth_key'], $user['password_hash'], $user['password_reset_token'], $user['created_at'], $user['updated_at'], $user['verification_token']);

        if ($user) {
            return $user;
        }

        return null;
    }

    /**
     * @SWG\Post(path="/user/feedback",
     *     tags={"Пользователь", "Обратная связь"},
     *     summary="Обратная связь",
     *     produces={"application/json", "application/xml"},
     *     security={{"Bearer":{}}},
     *
     *     @SWG\Parameter(
     *       in="formData",
     *       name="message",
     *       description="Сообщение",
     *       required=true,
     *       type="string",
     *       maxLength=1000,
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *         description=" success",
     *       @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="success", type="bool", example="true"),
     *       )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized",
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Error"
     *          )
     *     )
     * )
     *
     * @return FeedbackForm|array
     * @throws \Throwable
     */
    public function actionFeedback()
    {
        $model = new FeedbackForm([
            'user' => Yii::$app->user->getIdentity(),
        ]);

        if ($model->load(Yii::$app->request->post(), '')) {
            if (!$model->validate()) {
                return $model;
            }
            if ($model->send()) {
                return [
                    'success' => true,
                ];
            }

            return [
                'success' => false,
            ];
        }

        return $model;
    }

    /**
     * @SWG\Post(path="/user/change-password",
     *     tags={"Пользователь"},
     *     summary="Смена пароля пользователем",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *
     *     @SWG\Parameter(
     *        in="formData",
     *        name="password",
     *        description="Старый пароль",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="new_password",
     *        description="Новый пароль",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="password_repeat",
     *        description="Повторите пароль",
     *        required=true,
     *        type="string",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *            property="success",
     *            type="boolean",
     *            example=true,
     *          )
     *       )
     *    )
     * )
     * @return ChangePasswordForm|array
     */
    public function actionChangePassword()
    {
        $userId = Yii::$app->user->id;
        $model = User::findOne($userId);

        $changePasswordForm = new ChangePasswordForm($model);
        if ($changePasswordForm->load(Yii::$app->request->post(), '')) {
            if ($changePasswordForm->validate()) {
                $changePasswordForm->changePassword();

                return [
                    'success' => true,
                    'message' => 'Пароль успешно сменен',
                ];
            }

            return $changePasswordForm;
        }
    }

    /**
     * @SWG\Post(path="/user/request-password-reset",
     *     tags={"Пользователь"},
     *     summary="Запрос на смену пароля",
     *     produces={"applications/json"},
     *
     *     @SWG\Parameter(
     *       in="formData",
     *       name="email",
     *       type="string",
     *       required=true,
     *       description="Email",
     *     ),
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *       @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *            property="success",
     *            type="boolean",
     *            example=true,
     *         ),
     *         @SWG\Property(
     *           property="message",
     *           type="string",
     *           example="Проверьте свою электронную почту для дальнейших инструкций."
     *         )
     *      )
     *    )
     * )
     *
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post(), '')) {
            if ($model->validate()) {
                if ($model->sendEmail()) {
                    return [
                        'success' => true,
                        'message' => 'Проверьте свою электронную почту для дальнейших инструкций.',
                    ];
                } else {
                    return $model;
                }
            }
            return $model;
        }
    }


    /**
     * @SWG\Post(path="/user/set-push",
     *     tags={"Пользователь"},
     *     summary="Обновить push_token для пользователя",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *
     *     @SWG\Parameter(
     *        in="formData",
     *        name="push_token",
     *        description="Push token id",
     *        required=true,
     *        type="string",
     *    ),
     *    @SWG\Response(
     *        response="200",
     *        description="success",
     *       @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="success", type="bool", example="true"),
     *       )
     *    )
     * )
     */
    public function actionSetPush()
    {
        $userId = Yii::$app->user->id;
        $model = \common\models\User::findOne($userId);
        $model->scenario = User::SCENARIO_SET_PUSH_TOKEN;

        if (Yii::$app->request->isPost) {
            $pushToken = Yii::$app->request->post('push_token');
            $model->push_token = $pushToken;
            if ($model->save()) {
                return [
                    'success' => true,
                ];
            }

            return $model;
        }
    }

    /**
     * @SWG\Delete(path="/user/delete-push",
     *     tags={"Пользователь"},
     *     summary="Удалить push token id",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *          response="200",
     *         description=" success",
     *       @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="success", type="bool", example="true"),
     *       )
     *     ),
     * )
     * @return array|\common\models\User|null
     */
    public function actionDeletePush()
    {
        $userId = Yii::$app->user->id;
        $model = \common\models\User::findOne($userId);
        $model->scenario = User::SCENARIO_SET_PUSH_TOKEN;

        $model->push_token = null;

        if ($model->save()) {
            return [
                'success' => true,
            ];
        }

        return $model;
    }
}
