<?php

namespace api\modules\v1\controllers;

use api\common\controllers\ApiController;
use api\modules\v1\models\UserProfile;
use Swagger\Annotations as SWG;
use Yii;

/**
 * Class UserProfileController
 * @package api\modules\v1\controllers
 */
class UserProfileController extends ApiController
{
    public $modelClass = UserProfile::class;

    /**
     * @SWG\Get(path="/user-profile",
     *     tags={"Пользователь"},
     *     summary="Информация о текущем пользователе",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/UserProfile",
     *        )
     *     )
     * )
     *
     * @return UserProfile|bool|null
     */
    public function actionIndex()
    {
        return $this->findModel() ?? false;
    }

    public function actions()
    {
        return [
            'index',
        ];
    }

    /**
     * @return array
     */
    public function verbs()
    {
        return [];
    }

    /**
     * @SWG\Post(path="/user-profile",
     *     tags={"Пользователь"},
     *     summary="Изменение данных текущего пользователя",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *
     *     @SWG\Parameter(
     *        in="formData",
     *        name="name",
     *        description="Имя",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="surname",
     *        description="Фамилия",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        description="Телефон",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="address",
     *        description="Адрес",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="company_name",
     *        description="Название компании",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="inn",
     *        description="ИНН",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="_avatar",
     *        description="Аватар",
     *        required=false,
     *        type="file",
     *     ),
     *     @SWG\Parameter(
     *        in="formData",
     *        name="inn",
     *        description="ИНН",
     *        required=false,
     *        type="string",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *          type="object",
     *          ref="#/definitions/UserProfile",
     *        )
     *     )
     * )
     *
     * @return UserProfile|null
     * @throws \yii\base\Exception
     */
    public function actionUpdate()
    {
        $model = $this->findModel();
        $model->scenario = UserProfile::SCENARIO_UPDATE;


        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post(), '')) {
                if ($model->save()) {
                    return $model;
                }
            }
        }

        return $model;
    }

    /**
     * @return UserProfile|null
     */
    private function findModel()
    {
        $currentUserId = Yii::$app->user->id;
        $model = null;

        if ($currentUserId) {
            $model = UserProfile::findOne($currentUserId);
        }

        return $model;
    }
}
