<?php


namespace api\modules\v1\controllers;


use api\common\controllers\ApiController;
use common\models\Service;
use Swagger\Annotations as SWG;

/**
 * Class ServiceController
 *
 * @package api\modules\v1\controllers
 *
 */
class ServiceController extends ApiController
{
    public $modelClass = Service::class;

    /**
     * @SWG\Get(path="/service",
     *     tags={"Услуги"},
     *     summary="Услуги",
     *     description="Список услуг",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *         response = 200,
     *         description = "success",
     *         @SWG\Schema(
     *           type="array",
     *           @SWG\Items(
     *              type="object",
     *              ref="#/definitions/Service",
     *           )
     *        )
     *     )
     * )
     *
     */

    /**
     * @SWG\Get(path="/service/{id}",
     *     tags={"Услуги"},
     *     summary="Услуги",
     *     description="Список услуг",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *        in = "path",
     *        name = "id",
     *        description = "ID услуги",
     *        required = true,
     *        type = "integer"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = " success",
     *          @SWG\Schema(ref="#/definitions/Service")
     *     )
     * )
     *
     */
}
