<?php


namespace api\modules\v1\controllers;

use api\common\controllers\ApiController;
use common\models\Payment;
use common\models\PaymentOperation;
use Swagger\Annotations as SWG;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class PaymentController extends ApiController
{
    public $modelClass = Payment::class;

    public function actions()
    {
        $actions = parent::actions();

        $actions[] = 'pay';
        $actions[] = 'result';
        $actions[] = 'success';
        $actions[] = 'statuses';

        return $actions;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();

        $verbs['pay'] = ['POST'];

        return $verbs;
    }

    /**
     * @SWG\Get(path="/payment",
     *     tags={"Счет"},
     *     summary="Список счетов для обычного пользователя",
     *     description="",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *          response="200",
     *          description="success",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  ref="#/definitions/Payment",
     *              )
     *          )
     *     )
     * )
     */

    /**
     * @SWG\Get(path="/request/{id}/",
     *     tags={"Счет"},
     *     summary="Информация по счету",
     *     description="",
     *     @SWG\Parameter(
     *        in="path",
     *        name="id",
     *        description="ID счета",
     *        required=true,
     *        type="integer",
     *     ),
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *        @SWG\Schema(
     *           type="object",
     *           ref="#/definitions/Payment",
     *        ),
     *     ),
     *     @SWG\Response(
     *        response="403",
     *        description="Доступ запрещен",
     *     )
     * )
     */

    /**
     * @SWG\Post(path="/payment/pay/{id}",
     *     tags={"Счет"},
     *     summary="Оплата счета",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *       in="path",
     *       name="id",
     *       description="ID счета",
     *       required=true,
     *       type="integer",
     *     ),
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *
     *     )
     * )
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionPay($id)
    {
        $model = $this->findPayment($id);
        return $model->pay();
    }

    /**
     * @SWG\Get(path="/payment/result",
     *   tags={"Счет"},
     *     summary="Оповещение об оплате на ResultURL",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       in="path",
     *       name="InvId",
     *       description="ID заказа (ранее переданный в платежную систему)",
     *       required=true,
     *       type="integer",
     *     ),
     *     @SWG\Parameter(
     *       in="path",
     *       name="OutSum",
     *       description="Сумма, оплаченная покупателем",
     *       required=true,
     *       type="string",
     *     ),
     *     @SWG\Parameter(
     *       in="path",
     *       name="SignatureValue",
     *       description="Контрольная сумма",
     *       required=true,
     *       type="string",
     *     ),
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *     ),
     * )
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionResult()
    {
        $postData = \Yii::$app->request->post();
        $result = false;

        if (isset($postData['InvId'])) {
            $paymentId = $postData['InvId'];
        } else {
            throw new BadRequestHttpException('Неверные данные от платежной системы');
        }

        $model = $this->findPaymentOperation($paymentId);
        if ($model->checkResult($postData)) {
            $result = $model->setPaid();
        } else {
            return $model;
        }

        return [
            'success' => $result,
        ];
    }

    /**
     * @SWG\Get(path="/payment/success",
     *   tags={"Счет"},
     *     summary="Переадресация пользователя при успешной оплате на SuccessURL",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       in="path",
     *       name="InvId",
     *       description="ID заказа (ранее переданный в платежную систему)",
     *       required=true,
     *       type="integer",
     *     ),
     *     @SWG\Parameter(
     *       in="path",
     *       name="OutSum",
     *       description="Сумма, оплаченная покупателем",
     *       required=true,
     *       type="string",
     *     ),
     *     @SWG\Parameter(
     *       in="path",
     *       name="SignatureValue",
     *       description="Контрольная сумма",
     *       required=true,
     *       type="string",
     *     ),
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *     ),
     * )
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionSuccess()
    {
        $postData = \Yii::$app->request->post();
        if (isset($postData['InvId'])) {
            $paymentOperationId = $postData['InvId'];
        } else {
            throw new BadRequestHttpException('Неверные данные от платежной системы');
        }

        $model = $this->findPaymentOperation($paymentOperationId);

        return $model->setPaid();
    }

    /**
     * @SWG\Get(path="/payment/failed",
     *   tags={"Счет"},
     *     summary="Неуспешный возврат от платежной системы",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *       in="path",
     *       name="InvId",
     *       description="ID заказа (ранее переданный в платежную систему)",
     *       required=true,
     *       type="integer",
     *     ),
     *     @SWG\Response(
     *       response="200",
     *       description="success",
     *     ),
     * )
     *
     * @return array|PaymentOperation|null
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionFailed()
    {
        $postData = \Yii::$app->request->post();
        if (isset($postData['InvId'])) {
            $paymentOperationId = $postData['InvId'];
        } else {
            throw new BadRequestHttpException('Неверные данные от платежной системы');
        }

        $model = $this->findPaymentOperation($paymentOperationId);
        if (!$model->setFailed()) {
            return $model;
        }

        return [
            'success' => true,
            'message' => 'Вы отказались от оплаты. Заказ #' . $paymentOperationId,
        ];
    }

    /**
     * @SWG\Get(path="/payment/statuses",
     *     tags={"Счет"},
     *     summary="Статусы счета",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *        response="200",
     *        description="success",
     *          @SWG\Items(
     *              type="object",
     *              example={"1":"Не оплачено", "2":"Оплачено"}
     *          )
     *     )
     * )
     *
     * @return array
     */
    public function actionStatuses()
    {
        return Payment::getStatuses();
    }

    /**
     * @param $id
     * @return Payment
     * @throws NotFoundHttpException
     */
    protected function findPayment($id)
    {
        /** @var Payment $model */
        $model = Payment::find()
                        ->where(['id' => $id])
                        ->andWhere(['not', ['status' => Payment::STATUS_PAID]])
                        ->one();

        if ($model == null) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        return $model;
    }

    /**
     * @param $id
     * @return PaymentOperation|null
     * @throws NotFoundHttpException
     */
    protected function findPaymentOperation($id)
    {
        $model = PaymentOperation::findOne($id);

        if ($model == null) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        return $model;
    }
}
