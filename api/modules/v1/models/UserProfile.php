<?php

namespace api\modules\v1\models;

use yii\helpers\Json;

/**
 * Class UserProfile
 *
 * @package api\modules\v1\models
 */
class UserProfile extends \common\models\UserProfile
{
    public function rules()
    {
        return parent::rules();
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['address'] = function() {
            return $this->address;
        };

        return $fields;
    }
}
