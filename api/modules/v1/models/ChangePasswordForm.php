<?php


namespace api\modules\v1\models;


use yii\base\Model;

/**
 * Class ChangePasswordForm
 *
 * @package api\modules\v1\models
 *
 * @property string $password
 * @property string $new_password
 * @property string $password_repeat
 *
 * @property \common\models\User $_user
 */
class ChangePasswordForm extends Model
{
    public $password;
    public $new_password;
    public $password_repeat;

    private $_user;

    /**
     * ChangePasswordForm constructor.
     *
     * @param \common\models\User $user
     */
    public function __construct(\common\models\User $user)
    {
        $this->_user = $user;
        parent::__construct();
    }

    public function rules()
    {
        return [
            [['password', 'new_password', 'password_repeat'], 'required'],
            ['password', 'validatePassword'],
            ['new_password', 'compare', 'compareAttribute' => 'password_repeat', 'message' => 'Пароли должны совпадать'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'password_repeat' => 'Повтор пароля',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ((!$user || !$user->validatePassword($this->password))) {
                $this->addError($attribute, 'Пароль неверный.');
            }
        }
    }

    protected function getUser()
    {
        if (empty($this->_user)) {
            $this->addError('Пользователь не найден');

            return null;
        }

        return $this->_user;
    }

    public function changePassword()
    {
        $this->_user->scenario = \common\models\User::SCENARIO_CHANGE_PASSWORD;
        $this->_user->setPassword($this->new_password);
        $this->_user->save();
    }
}
