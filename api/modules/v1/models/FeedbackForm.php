<?php


namespace api\modules\v1\models;


use common\helpers\MailHelper;
use common\helpers\models\Letter;
use yii\base\Model;
use yii\helpers\Html;

/**
 * Class FeedbackForm
 *
 * @property \api\common\models\User $user
 *
 * @package api\modules\v1\models
 */
class FeedbackForm extends Model
{
    public $user;
    public $message;

    public function rules()
    {
        return [
            ['message', 'required'],
            ['message', 'string', 'max' => 1000, 'min' => 10],
            ['message', 'trim'],
        ];
    }

    public function send()
    {
        $username = $this->user->userProfile->getFullName();
        $userEmail = $this->user->email;
        $body = "Новое сообщение от $username ($userEmail)\n";
        $body .= "-----------\n";
        $body .= Html::encode($this->message)."\n";
        $body .= "------------\n";
        $letter = new Letter([
            'subject' => 'Новое сообщение из приложения УРПИ',
            'body' => $body,
        ]);

        return MailHelper::sendToSupport($letter);
    }
}
