<?php

namespace api\modules\v1\models;

use api\common\models\Token;
use Yii;
use yii\base\InvalidArgumentException;

/**
 * Class LoginForm
 * @package api\modules\v1\models
 */
class LoginForm extends \common\models\LoginForm
{
    /**
     * @return Token|null
     */
    public function login()
    {
        if ($this->validate()) {
            $token = new Token();
            $user = $this->getUser();
            $token->user_id =$user->id;
            $expireDays = (int)Yii::$app->params['token.expireTime'];
            if ($expireDays < 0) {
                throw new InvalidArgumentException('token.expireTime should be more than 0');
            }
            $token->generateToken(time() + $expireDays * 3600 * 24);
            $fullName = '';
            if ($user->userProfile) {
                $fullName = $user->userProfile->getFullName();
            }
            $data = [
                'token' => $token->access_token,
                'type' => $user->type,
                'fullName' => $fullName,
            ];

            return $token->save() ? $data : null;
        } else {
            return null;
        }
    }
}
