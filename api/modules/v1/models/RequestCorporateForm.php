<?php

namespace api\modules\v1\models;

use common\models\form\ImagesUploadForm;

/**
 * Class RequestCorporateForm
 * @package api\modules\v1\models
 * @deprecated No need more. Use Cart class instead.
 * @see Cart
 */
class RequestCorporateForm extends ImagesUploadForm
{
    public $content;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['content'], 'trim'],
            [
                ['uploadFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => $this->extensions,
                'checkExtensionByMimeType' => false,
                'maxFiles' => 4,
            ],
        ];
    }
}
