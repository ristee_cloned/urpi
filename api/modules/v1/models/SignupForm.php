<?php

namespace api\modules\v1\models;

use yii\base\Model;
use Yii;

/**
 * Class SignupForm
 * @package api\modules\v1\models
 *
 *
 * @SWG\Definition(definition="SignupForm", type="object",
 *     required={"email", "password", "type", "name", "surname", "phone", "address"},
 *      @SWG\Property(property="id", type="integer", example=123),
 *      @SWG\Property(property="email", type="string", example="ivanov@example.ru"),
 *      @SWG\Property(property="password", type="string", example="123123"),
 *      @SWG\Property(property="type", type="integer", example=1),
 *      @SWG\Property(property="name", type="string", example="Иван"),
 *      @SWG\Property(property="surname", type="string", example="Иванов"),
 *      @SWG\Property(property="phone", type="string", example="81231231231"),
 *      @SWG\Property(property="address", type="string", example="г. Екатеринбург"),
 *      @SWG\Property(property="company", type="string", example="ООО Святогор"),
 *      @SWG\Property(property="inn", type="string", example="66012345"),
 *  )
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $type;
    public $name;
    public $surname;
    public $phone;
    public $address;
    public $company;
    public $inn;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => 'api\modules\v1\models\User',
                'message' => 'Этот адрес электронной почты уже занят.',
            ],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['type', 'required'],
            ['type', 'integer'],
            [['name', 'surname', 'phone', 'address'], 'required'],
            [['name', 'surname'], 'string', 'max' => 50],
            [
                [
                    'address',
                    'company',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'name',
                    'surname',
                    'address',
                    'inn',
                    'company',
                ],
                'trim'
            ],
            [
                ['name', 'surname',],
                'match',
                'pattern' => '/^[а-яёa-z]+$/iu',
                'message' => 'Пожалуйста, указывайте только буквы.'
            ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->email = $this->email;
            $user->type = $this->type;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();

            if (!$user->save()) {
                return null;
            }

            $profile = new UserProfile();
            $profile->id = $user->id;
            $profile->name = $this->name;
            $profile->surname = $this->surname;
            $profile->phone = $this->phone;
            $profile->address = $this->address;
            $profile->company_name = $this->company;
            $profile->inn = $this->inn;

            return $profile->save() && $this->sendEmail($user);

        } else {
            return null;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                [
                    'html' => 'api/emailVerify-html',
                    'text' => 'api/emailVerify-text',
                ],
                ['user' => $user]
            )
            ->setFrom(
                [Yii::$app->params['supportEmail'] => Yii::$app->name . ' робот']
            )
            ->setTo($this->email)
            ->setSubject('Регистрация аккаунта на ' . Yii::$app->name)
            ->send();
    }
}
