<?php

namespace api\modules\v1\models;

use yii\web\Linkable;
use yii\web\Link;
use yii\helpers\Url;

/**
 * Class User
 * @package api\modules\v1\models
 */
class User extends \api\common\models\User implements Linkable
{
    /**
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();
        unset(
            $fields['auth_key'],
            $fields['password_hash'],
            $fields['password_reset_token']
        );

        return $fields;
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return ['userProfile'];
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(
                [
                    'user/view',
                    'id' => $this->id
                ],
                true
            ),
        ];
    }
}
