<?php

namespace api\modules\v1\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * Class VerifyEmailForm
 * @package api\modules\v1\models
 */
class VerifyEmailForm extends Model
{
    public $token;

    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'string'],
            ['token', 'trim'],
        ];
    }

    /**
     * @return bool|null|string
     */
    public function verifyEmail()
    {
        if ($this->validate()) {
            $user = User::findByVerificationToken($this->token);
            if (!$user) {
                throw new InvalidArgumentException(
                    'Неправильно подтвержден токен электронной почты.'
                );
            }
            $user->status = User::STATUS_ACTIVE;
            return $user->save() ? true : null;
        } else {
            return null;
        }
    }
}
