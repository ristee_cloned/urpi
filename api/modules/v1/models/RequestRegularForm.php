<?php

namespace api\modules\v1\models;

use common\models\form\ImagesUploadForm;

/**
 * Class RequestRegularForm
 * @package api\modules\v1\models
 * @deprecated No need more. Use Cart class instead.
 * @see Cart
 */
class RequestRegularForm extends ImagesUploadForm
{

}
