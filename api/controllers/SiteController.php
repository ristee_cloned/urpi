<?php

namespace app\controllers;


use api\modules\v1\models\VerifyEmailForm;
use frontend\models\ResetPasswordForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ServiceController
 *
 * @package api\modules\v1\controllers
 *
 */
class SiteController extends Controller
{
    public function actions()
    {
        $actions = [
            'docs' => [
                'class' => 'light\swagger\SwaggerAction',
                'restUrl' => Url::to(['/site/api'], true),
            ],
            'api' => [
                'class' => 'light\swagger\SwaggerApiAction',
                //The scan directories, you should use real path there.
                'scanDir' => [
                    Yii::getAlias('@api/modules/v1/swagger'),
                    Yii::getAlias('@api/modules/v1/controllers'),
                    Yii::getAlias('@api/modules/v1/models'),
                    Yii::getAlias('@common/models'),
                ],
//                'cache' => 'cache',
                'cacheKey' => '',
                //The security key
//                'api_key' => 'blablacar',
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];

        if (YII_ENV == 'prod') {
            unset($actions['docs'], $actions['api']);
        }

        return $actions;
    }

    /**
     * @return VerifyEmailForm|bool
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        $model = new VerifyEmailForm();
        $model->load(Yii::$app->request->queryParams, '');
        try {
            if ($model->verifyEmail()) {
                return true;
            }
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        return $model;
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранен.');

            $frontend = Yii::$app->params['frontendBaseUrl'];

            return 'Пароль успешно сброшен. Вы можете войти в приложение с новым паролем.';
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}