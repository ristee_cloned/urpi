<?php


namespace api\components;


use common\models\Payment;
use common\models\PaymentOperation;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\helpers\StringHelper;

/**
 * Class Robokassa
 *
 * @package api\components
 *
 * @property string $login
 * @property string $password
 * @property string $password2
 * @property string $isTest
 *
 * @property Payment $payment
 * @property int $orderNumber
 * @property string $description
 * @property int $outSumm
 *
 *
 */
class Robokassa extends Model
{
    const DESCRIPTION_LENGTH = 100;
    const DESCRIPTION_TEMPLATE =  "Плата за услуги Уралпроинжениринг. Заказ {{orderNumber}}";

    //login, password
    private $login;
    private $password;
    private $password2;
    private $isTest = false;

    private $payment = null;


    public function init()
    {
        parent::init();
        $this->login = Yii::$app->params['robokassa.login'];
        $this->password = Yii::$app->params['robokassa.password'];
        $this->password2 = Yii::$app->params['robokassa.password2'];
        $this->isTest = Yii::$app->params['robokassa.isTest'];
    }

    //number of order
    private $orderNumber = null;

    // description
    private $description = "Плата за услуги Уралпроминжениринг.";

    // sum of order
    public $outSumm = 0;

    //code of goods
    public $shp_item = "2"; //not used yet

    //default payment e-currency
    private $in_curr = '';  //not used yet

    //language
    private $culture = 'ru';  //not used yet

    public function getUrl()
    {
        $description = $this->getDescription();
        $signature = $this->getSignature();
        $outSumm = $this->getSummToUrl();

        $url = "https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin={$this->login}&".
            "OutSum={$outSumm}&InvId={$this->orderNumber}&Description={$description}";

        if ($this->isTest) {
            $url.="&IsTest=1";
        }

        $url .= "&SignatureValue=$signature";

        return $url;
    }

    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
        $this->outSumm = $payment->total_price;
    }

    public function setOrderNumber(PaymentOperation $paymentOperation)
    {
        $this->orderNumber = $paymentOperation->id;
    }

    public function getSignature()
    {
        if (empty($this->orderNumber)) {
            throw new InvalidArgumentException('"order_number" should be set.');
        }

        $outSumm = $this->getSummToUrl();

        $str = "$this->login:$outSumm:$this->orderNumber:$this->password";

        return md5($str);
    }

    public function getDescription()
    {
        $this->description = strtr(self::DESCRIPTION_TEMPLATE, [
            '{{orderNumber}}' => $this->payment->id,
        ]);
        if (mb_strlen($this->description) > 100) {
            $this->description = StringHelper::truncate($this->description, self::DESCRIPTION_LENGTH, '');
        }

        return $this->description;
    }

    private function getSummToUrl()
    {
        return (int)$this->outSumm.'.00';
    }

    public function checkResult($postData)
    {
        if (!isset($postData['OutSum']) || isset($postData['InvId']) || isset($postData['SignatureValue'])) {
            $this->addError('password2', 'Неверный ответ от платежной системы.');
            return false;
        }

        $outSum = $postData['OutSum'];
        $orderNumber = $postData['InvId'];
        $signature = strtoupper($postData['SignatureValue']);

        $str = "$outSum:$orderNumber:{$this->password2}";
        $checkSignature = strtoupper(md5($str));

        if ($checkSignature != $signature) {
            $this->addError('signature', 'Неверная подпись');
            return false;
        }

        return true;
    }

    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    public function getPaymentId()
    {

        if ($this->payment) {
            return $this->payment->id;
        }

        return null;
    }
}
