<?php

namespace api\common\controllers;

use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class ApiController
 * @package api\common\controllers
 */
class ApiController extends ActiveController
{
    public $exceptAuthRoute = [
        'v1/user/login',
        'v1/user/signup',
        'v1/user/request-password-reset',
        'v1/payment/success',
        'v1/payment/fail',
        'v1/payment/result',
    ];

    public $exceptUnsetRoute = [
        'v1/user/login',
        'v1/user/signup',
        'v1/user-profile/update',
    ];
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if (!in_array(\Yii::$app->controller->route, $this->exceptAuthRoute)) {
            $behaviors['authenticator']['authMethods'] = [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
            ];

            $behaviors['access'] = [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ];
        }

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        if (!in_array(\Yii::$app->controller->route, $this->exceptUnsetRoute)) {
            unset($actions['update'], $actions['delete'], $actions['create']);
        }

        return $actions;
    }
}
