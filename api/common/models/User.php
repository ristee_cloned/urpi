<?php

namespace api\common\models;

/**
 * Class User
 * @package api\common\models
 */
class User extends \common\models\User
{
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $identity = static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.access_token' => $token])
            ->andWhere(['>', 't.expired_at', time()])
            ->one();

        $identity ?: Token::deleteByAccessToken($token);

        return $identity;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::class, ['user_id' => 'id']);
    }
}
