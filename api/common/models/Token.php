<?php

namespace api\common\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "token".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property int $expired_at
 *
 * @property User $user
 */
class Token extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%token}}';
    }

    /**
     * @param $token
     * @return null|static
     */
    public static function findByAccessToken($token)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * @param $token
     */
    public static function deleteByAccessToken($token)
    {
        if ($token = self::findByAccessToken($token)) {
            $token->delete();
        }
    }

    /**
     * @param $expire
     */
    public function generateToken($expire)
    {
        $this->expired_at = $expire;
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'token' => 'access_token',
            'expired' => function () {
                return date(DATE_RFC3339, $this->expired_at);
            },
        ];
    }
}
